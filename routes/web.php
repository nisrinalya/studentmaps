<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('login.index');
});*/

Route::get('/', 'Backend\BackendController@nilaiAll')->middleware('verify-login');
Route::get('login', 'LoginController@index');
Route::get('logout', 'LoginController@logout');
Route::post('validate-login', 'LoginController@login');

Route::group(['prefix' => 'backend', 'namespace' => 'Backend', 'middleware' => 'verify-login'], function(){
	Route::get('/', 'BackendController@nilaiAll');
	Route::get('chart', 'BackendController@chart');
});

Route::group(['prefix' => 'frontend', 'namespace' => 'Frontend'], function(){
	Route::get('/', 'FrontendController@index');
	Route::get('student-stats', 'StudentController@viewChart');
	Route::get('student-chart', 'StudentController@chart');
	Route::get('school-stats', 'SchoolController@viewChart');
	Route::get('school-chart', 'SchoolController@chart');
	Route::get('promotion-stats', 'PromotionController@viewChart');
	Route::get('promotion-chart', 'PromotionController@chart');
	Route::get('get-data', 'FrontendController@dataMaps');
	Route::get('maps', 'FrontendController@showMaps');
});

Route::group(['prefix' => 'student', 'middleware' => 'verify-login'], function(){
	Route::get('/', 'Backend\StudentController@index');
	Route::get('get-option', 'Backend\StudentController@loadData');
	Route::get('getdistrict/{id}', 'Backend\StudentController@loadDistrict');
	Route::get('gethighschool/{id}', 'Backend\StudentController@loadHighschool');
	Route::get('add', 'Backend\StudentController@add');
    Route::post('add-import', 'Backend\StudentController@form');
    Route::post('save-import', 'Backend\StudentController@saveImport');
	Route::get('chart', 'Backend\StudentController@chartMahasiswa');
	Route::get('statistic', 'Backend\StudentController@viewChart');
});

Route::group(['prefix' => 'school', 'middleware' => 'verify-login'], function(){
	Route::get('/', 'Backend\SchoolController@index');
	Route::get('add', 'Backend\SchoolController@add');
	Route::get('chart', 'Backend\SchoolController@chartSekolah');
	Route::get('statistic', 'Backend\SchoolController@viewChart');
});

Route::group(['prefix' => 'promotion', 'middleware' => 'verify-login'], function(){
	Route::get('/', 'Backend\PromotionController@index');
	Route::get('add', 'Backend\PromotionController@add');
	Route::get('chart', 'Backend\PromotionController@chartSosialisasi');
	Route::get('statistic', 'Backend\PromotionController@viewChart');
});

Route::group(['prefix' => 'recommendation', 'middleware' => 'verify-login'], function()
{
	Route::get('result', 'Backend\FuzzyController@index');
	Route::get('result2', 'Backend\FuzController@indexNew');
    Route::get('get-data', 'Backend\FuzController@dataMaps');
	Route::post('show', 'Backend\FuzController@query');
    Route::get('excel', 'Backend\FuzController@excel');
});