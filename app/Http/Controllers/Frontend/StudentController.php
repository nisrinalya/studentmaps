<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Student;
use App\Models\City;
use App\Models\District;
use App\Models\School;

class StudentController extends Controller
{
    public function chart()
    {
    	$city = City::all();
        $nullData = [];
        $currData = [];
        $chartMahasiswa = [];

        foreach ($city as $key => $value) {
            $data = Student::join('kecamatan', 'mahasiswa.id_kecamatan', '=', 'kecamatan.id')->where('kecamatan.id_kota', $value->id)->count();
            if (is_null($data)) {
                $nullData[] = [
                    'id_kota' => $value->id,
                    'nama_kota' => $value->nama,
                    'count' => 0
                ];
            } else {
                $currData[] = [
                    'id_kota' => $value->id,
                    'nama_kota' => $value->nama,
                    'count' => $data
                ];
            }
        }
 
        $countData = array_merge($currData, $nullData);
 
        foreach ($countData as $key => $item) {
            $chartMahasiswa[] = [
                'city' => $item['nama_kota'],
                'name' => $item['id_kota'],
                'y' => $item['count']
            ];
        }
 
        $params = [
            'data' => $chartMahasiswa
        ];

        return response()->json($params);
    }

    public function viewChart()
    {
    	return view('students.frontstats');
    }
}
