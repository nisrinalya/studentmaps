<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Student;
use App\Models\City;
use App\Models\District;
use App\Models\School;
use App\Models\Promotion;

class FrontendController extends Controller
{
    public function index()
    {
    	return view('fronthome.index');
    }

    public function studentStats()
    {
    	return view('students.frontstats');
    }

    public function showMaps(Request $request)
    {
        $year=$request->year;
        if(is_null($year) || $year==''){
            $year=date('Y');
        }
    	$params = [
    		'dataMaps' => $this->dataMaps($year)
    	];
    	return view('recommendation.frontmaps', $params);
    }

    public function dataMaps($year)
    {
        $dataSekolah = $this->nilaiSekolah($year);
        $dataMahasiswa = $this->nilaiMahasiswa($year);
        $dataSosialisasi = $this->nilaiSosialisasi($year);
        $city = City::all();
        $dataMaps = [];

        foreach ($city as $key => $value) {
            if ($value->id == $dataSekolah[$key]['id_kota'] && $value->id == $dataMahasiswa[$key]['id_kota'] && $value->id == $dataSosialisasi[$key]['id_kota']) {
                $dataMaps[] = [
                    'id_kota' => $value->id,
                    'nama_kota' => $value->nama,
                    'count_sekolah' => $dataSekolah[$key]['count'],
                    'count_mahasiswa' => $dataMahasiswa[$key]['count'],
                    'count_sosialisasi' => $dataSosialisasi[$key]['count']
                ];
            }
        }

        return $dataMaps;
    }

    public function nilaiAll($type,$year)
	{
        $city = City::all();
        $nullData = [];
        $currData = [];

        foreach ($city as $key => $value) {
            if ($type == 1) {
                $data = School::join('kecamatan', 'sma.id_kecamatan', '=', 'kecamatan.id')->where('kecamatan.id_kota', $value->id)->count();
            } elseif ($type == 2) {
                $data = Student::join('kecamatan', 'mahasiswa.id_kecamatan', '=', 'kecamatan.id')->where('kecamatan.id_kota', $value->id)
                    ->where('mahasiswa.angkatan',$year)
                    ->count();
            } elseif ($type == 3) {
                $data = Promotion::where('id_kota', $value->id)
                    ->where('ajaran_akhir',$year)
                    ->count();
            }

            if (is_null($data)) {
                $nullData[] = [
                    'id_kota' => $value->id,
                    'nama_kota' => $value->nama,
                    'count' => 0
                ];
            } else {
                $currData[] = [
                    'id_kota' => $value->id,
                    'nama_kota' => $value->nama,
                    'count' => $data
                ];
            }
        }

        $countData = array_merge($currData, $nullData);

        return $countData;
	}

	public function nilaiSekolah($year)
    {
    	$derajatSekolah = [];

    	$countData = $this->nilaiAll(1,$year);

    	foreach ($countData as $key => $item) {
    		$derajatSekolah[] = [
    			'id_kota' => $item['id_kota'],
    			'nama_kota' => $item['nama_kota'],
    			'count' => $item['count']
    		];
    	}

    	return $derajatSekolah;
    }

	public function nilaiMahasiswa($year)
    {
    	$derajatMahasiswa = [];

    	$countData = $this->nilaiAll(2,$year);

    	foreach ($countData as $key => $item) {
    		$derajatMahasiswa[] = [
    			'id_kota' => $item['id_kota'],
    			'nama_kota' => $item['nama_kota'],
    			'count' => $item['count']
    		];
    	}

    	return $derajatMahasiswa;
    }

    public function nilaiSosialisasi($year)
    {
    	$derajatSosialisasi = [];

    	$countData = $this->nilaiAll(3,$year);

    	foreach ($countData as $key => $item) {
    		$derajatSosialisasi[] = [
    			'id_kota' => $item['id_kota'],
    			'nama_kota' => $item['nama_kota'],
    			'count' => $item['count']
    		];
    	}

    	return $derajatSosialisasi;
    }
}
