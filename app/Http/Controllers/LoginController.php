<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Session;

class LoginController extends Controller
{
    public function index(Request $request)
    {
    	if ($request->session()->exists('activeUser')) {
    		return redirect('/');
    	}
    	return view('login.index');
    }

    public function login(Request $request)
    {
    	$username = $request->input('username');
    	$password = $request->input('password');

    	$activeUser = User::where(['username'=>$username, 'password'=>sha1($password)])->first();

    	if (is_null($activeUser)) {
    		echo "<div class='alert alert-danger'><strong>Login failed.</strong> User not found.</div>";
    		return view('login.index');
    	}

		if ($activeUser->password==sha1($password)) {
			$request->session()->put('activeUser', $activeUser);
			return redirect('/');
		} else {
			echo "<div class='alert alert-danger'><strong>Login failed.</strong> Please check your username or password.</div>";
			return view('login.index');
		}
    }

    public function logout(Request $request)
    {
    	Session::flush();
    	return redirect('/');
    }
}
