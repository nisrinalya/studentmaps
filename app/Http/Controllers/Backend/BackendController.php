<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Student;
use App\Models\City;
use App\Models\District;
use App\Models\School;
use App\Models\Promotion;

class BackendController extends Controller
{
    public function index(Request $request)
    {
    	return view('dashboard.index');
    }

    public function nilaiAll()
    {
    	$resultSekolah = $this->nilaiSekolah();
		$resultMahasiswa = $this->nilaiMahasiswa();
		$resultSosialisasi = $this->nilaiSosialisasi();
		$studentSum = $this->totalData();
		$city = City::all();
		$result = [];

    	foreach ($city as $key => $value) {
    		if ($value->id == $resultSekolah[$key]['id_kota'] && $value->id == $resultMahasiswa[$key]['id_kota'] && $value->id == $resultSosialisasi[$key]['id_kota']) {
	    		$result[] = [
					'nama_kota' => $value->nama,
					'count_sekolah' => $resultSekolah[$key]['count'],
					'count_mahasiswa' => $resultMahasiswa[$key]['count'],
					'count_sosialisasi' => $resultSosialisasi[$key]['count']
				];
	    	}
    	}

    	$resultSkl = $this->arrayOrderBy($result, 'count_sekolah desc');
    	$resultMhs = $this->arrayOrderBy($result, 'count_mahasiswa desc');
    	$resultSos = $this->arrayOrderBy($result, 'count_sosialisasi desc');

    	$params = [
			'result_sekolah' => array_slice($resultSkl,0,5),
			'result_mahasiswa' => array_slice($resultMhs,0,5),
			'result_sosialisasi' => array_slice($resultSos,0,5),
			'total_mahasiswa' => $studentSum['jml_mahasiswa'],
			'total_sosialisasi' => $studentSum['jml_sosialisasi'],
			'total_sekolah' => $studentSum['jml_sekolah']
		];

		// dd($params);
		return view('dashboard.index', $params);
    }

	private function arrayOrderBy(array &$arr, $order = null) {
	    if (is_null($order)) {
	        return $arr;
	    }
	    $orders = explode(',', $order);
	    usort($arr, function($a, $b) use($orders) {
	        $result = array();
	        foreach ($orders as $value) {
	            list($field, $sort) = array_map('trim', explode(' ', trim($value)));
	            if (!(isset($a[$field]) && isset($b[$field]))) {
	                continue;
	            }
	            if (strcasecmp($sort, 'desc') === 0) {
	                $tmp = $a;
	                $a = $b;
	                $b = $tmp;
	            }
	            if (is_numeric($a[$field]) && is_numeric($b[$field]) ) {
	                $result[] = $a[$field] - $b[$field];
	            } else {
	                $result[] = strcmp($a[$field], $b[$field]);
	            }
	        }
	        return implode('', $result);
	    });
	    return $arr;
	}

	public function chart()
	{
		$chartSosialisasi = [];

    	$countData = $this->hitungNilai(3);

    	foreach ($countData as $key => $item) {
    		$chartSosialisasi[] = [
                'city' => $item['nama_kota'],
    			'name' => $item['id_kota'],
    			'y' => $item['count']
    		];
    	}

    	$params = [
    		'data' => $chartSosialisasi
    	];

    	return response()->json($params);
	}

	public function totalData()
	{
		$studentSum = Student::all()->count();
		$promotionSum = Promotion::all()->count();
		$schoolSum = School::all()->count();

		$params = [
			'jml_mahasiswa' => $studentSum,
			'jml_sosialisasi' => $promotionSum,
			'jml_sekolah' => $schoolSum
		];

		return $params;
	}

    public function hitungNilai($type)
    {
    	$city = City::all();
    	$nullData = [];
    	$currData = [];

    	foreach ($city as $key => $value) {
    		if ($type == 1) {
    			$data = School::join('kecamatan', 'sma.id_kecamatan', '=', 'kecamatan.id')->where('kecamatan.id_kota', $value->id)->count();
    		} elseif ($type == 2) {
    			$data = Student::join('kecamatan', 'mahasiswa.id_kecamatan', '=', 'kecamatan.id')->where('kecamatan.id_kota', $value->id)->count();
    		} elseif ($type == 3) {
    			$data = Promotion::where('id_kota', $value->id)->count();
    		}

    		if (is_null($data)) {
    			$nullData[] = [
    				'id_kota' => $value->id,
    				'nama_kota' => $value->nama,
    				'count' => 0
    			];
    		} else {
    			$currData[] = [
    				'id_kota' => $value->id,
    				'nama_kota' => $value->nama,
    				'count' => $data
    			];
    		}
    	}

    	$countData = array_merge($currData, $nullData);

    	return $countData;
    }

    public function nilaiSekolah()
    {
    	$nilaiSekolah = [];

    	$countData = $this->hitungNilai(1);

    	foreach ($countData as $key => $item) {
    		$nilaiSekolah[] = [
    			'id_kota' => $item['id_kota'],
    			'nama_kota' => $item['nama_kota'],
    			'count' => $item['count']
    		];
    	}

    	return $nilaiSekolah;
    }

    public function nilaiMahasiswa()
    {
    	$nilaiMahasiswa = [];

    	$countData = $this->hitungNilai(2);

    	foreach ($countData as $key => $item) {
    		$nilaiMahasiswa[] = [
    			'id_kota' => $item['id_kota'],
    			'nama_kota' => $item['nama_kota'],
    			'count' => $item['count']
    		];
    	}

    	return $nilaiMahasiswa;
    }

    public function nilaiSosialisasi()
    {
    	$nilaiSosialisasi = [];

    	$countData = $this->hitungNilai(3);

    	foreach ($countData as $key => $item) {
    		$nilaiSosialisasi[] = [
    			'id_kota' => $item['id_kota'],
    			'nama_kota' => $item['nama_kota'],
    			'count' => $item['count']
    		];
    	}

    	return $nilaiSosialisasi;
    }
}
