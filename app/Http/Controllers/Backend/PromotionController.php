<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\Promotion;

class PromotionController extends Controller
{
    public function index(Request $request)
    {
    	$data = Promotion::all();
    	$params = [
    		'data' => $data
    	];

    	return view('promotions.index', $params);
    }

    public function add(Request $request)
    {
    	return view('promotions.form');
    }

    public function chartSosialisasi(Request $request)
    {
        $city = City::all();
        $nullData = [];
        $currData = [];
        $chartSosialisasi = [];

        foreach ($city as $key => $value) {
            $data = Promotion::where('id_kota', $value->id)->count();
            if (is_null($data)) {
                $nullData[] = [
                    'id_kota' => $value->id,
                    'nama_kota' => $value->nama,
                    'count' => 0
                ];
            } else {
                $currData[] = [
                    'id_kota' => $value->id,
                    'nama_kota' => $value->nama,
                    'count' => $data
                ];
            }
        }
 
        $countData = array_merge($currData, $nullData);
 
        foreach ($countData as $key => $item) {
            $chartSosialisasi[] = [
                'city' => $item['nama_kota'],
                'name' => $item['id_kota'],
                'y' => $item['count']
            ];
        }
 
        $params = [
            'data' => $chartSosialisasi
        ];

        return response()->json($params);
    }

    public function viewChart()
    {
        return view('promotions.stats');
    }
}
