<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Student;
use App\Models\City;
use App\Models\District;
use App\Models\School;
use Excel;
use Illuminate\Support\Facades\DB;

class StudentController extends Controller
{
    public function index(Request $request)
    {
    	$data = Student::all();
    	$params = [
    		'data' => $data
    	];

    	return view('students.index', $params);
    }

    public function form(Request $request)
    {
        $params = [
            'title' => 'Import Data Mahasiswa'
        ];

        return view('students.add-import', $params);
    }

    public function saveImport(Request $request)
    {
        if ($request->hasFile('file_mahasiswa')) {
            $path = $request->file('file_mahasiswa')->getRealPath();
            $data = Excel::load($path, function ($reader) {})->get();
            if (!empty($data) && $data->count()) {
                foreach ($data as $key => $item) {
                    if (!empty($item)) {
                        try {
                            $checkKecamatan=District::where(['id'=>$item->id_kecamatan])->first();
                            if(!$checkKecamatan){
                                return "<div class='alert alert-danger'>ID Kecamatan $item->id_kecamatan tidak ditemukan!</div>";
                            }
                            $cekMahasiswa=Student::where(['nrp'=>$item->nrp])->first();
                            if($cekMahasiswa){
                                return "<div class='alert alert-danger'>NRP $item->nrp sudah ada!</div>";
                            }
                            $insert[]=[
                                'nrp'=>$item->nrp,
                                'nama'=>$item->nama,
                                'angkatan'=>$item->angkatan,
                                'program_studi'=>$item->program_studi,
                                'jenis_kelamin'=>$item->jenis_kelamin,
                                'alamat'=>$item->alamat,
                                'id_kecamatan'=>$item->id_kecamatan,
                                'sma'=>$item->sma,
                                'jalur_penerimaan'=>$item->jalur_penerimaan
                            ];


                        } catch (\Exception $e) {
                            return "<div class='alert alert-danger'>Format Excel Tidak Valid!</div>";
                        }
                    } else {
                        return "<div class='alert alert-danger'>Data yang akan diimport berisi kosong!</div>";
                    }
                }
                DB::beginTransaction();
                try {
                    Student::insert($insert);
                    DB::commit();

                    return "
            <div class='alert alert-success'>Data berhasil diimport!</div>
            <script> scrollToTop(); reload(1000); </script>";
                } catch (\Exception $e) {
                    DB::rollBack();

                    return "<div class='alert alert-danger'>Data  gagal diimport!</div>";
                }
            } else {
                return "<div class='alert alert-danger'>Data yang akan diimport kosong!</div>";
            }
        } else {
            return "<div class='alert alert-danger'>File tidak Valid!</div>";
        }
    }

    public function loadData(Request $request)
    {
        $id_kota = $request->get('id_kota');
        $result = District::where(['id_kota'=>$id_kota])->get();

        if ($result) {
            $options = [];

            foreach ($result as $num => $item) {
                $options[] = [
                    'id' => $item->id_kecamatan,
                    'text' => $item->nama
                ];
            }
        } else {
            $options = [];
        }

        return response()->json($options);
    }

    public function loadDistrict($id)
    {
        $district = District::where('id_kota', $id)->get();

        return json_encode($district);
    }

    public function loadHighschool($id)
    {
        $highschool = School::where('id_kecamatan', $id)->get();

        return json_encode($highschool);
    }

    public function add(Request $request)
    {
    	$data = Student::all();
        $kota = City::get();
        $kecamatan = District::get();
        $sekolah = School::all();
    	$params = [
    		'data' => $data,
            'kota' => $kota,
            'kecamatan' => $kecamatan,
            'sekolah' => $sekolah
    	];

    	return view('students.form2', $params);
    }

    public function save(Request $request)
    {
    	$nrp = $request->input('nrp');
    	$nama = $request->input('nama');
    	$program_studi = $request->input('program_studi');
    	$jenis_kelamin = $request->input('jenis_kelamin');
    	$alamat = $request->input('alamat');
    	$id_kecamatan = $request->input('id_kecamatan');
    	$sma = $request->input('sma');
    	$jalur_penerimaan = $request->input('jalur_penerimaan');

    	$student = new Student;
    	$student->nrp = $nrp;
    	$student->nama = $nama;
    	$student->program_studi = $program_studi;
    	$student->jenis_kelamin = $jenis_kelamin;
    	$student->alamat = $alamat;
    	$student->id_kecamatan = $id_kecamatan;
    	$student->sma = $sma;
    	$student->jalur_penerimaan = $jalur_penerimaan;

    	try {
    		$student->save();
    		return "<div class='alert alert-success'><strong>It's done!</strong> You have successfully added new data.</div>
    				<script>scrollToTop(); reload(1500);</script>";
    	} catch (\Exception $e) {
    		dd($e);
    		return "<div class='alert alert-danger'><strong>Oops, there is something wrong!</strong> Please check your data input again.</div>";
    	}

    	//return ('students.index');
    }

    public function chartMahasiswa(Request $request)
    {
        $city = City::all();
        $nullData = [];
        $currData = [];
        $chartMahasiswa = [];

        foreach ($city as $key => $value) {
            $data = Student::join('kecamatan', 'mahasiswa.id_kecamatan', '=', 'kecamatan.id')->where('kecamatan.id_kota', $value->id)->count();
            if (is_null($data)) {
                $nullData[] = [
                    'id_kota' => $value->id,
                    'nama_kota' => $value->nama,
                    'count' => 0
                ];
            } else {
                $currData[] = [
                    'id_kota' => $value->id,
                    'nama_kota' => $value->nama,
                    'count' => $data
                ];
            }
        }
 
        $countData = array_merge($currData, $nullData);
 
        foreach ($countData as $key => $item) {
            $chartMahasiswa[] = [
                'city' => $item['nama_kota'],
                'name' => $item['id_kota'],
                'y' => $item['count']
            ];
        }
 
        $params = [
            'data' => $chartMahasiswa
        ];
 
        // dd($params);
        return response()->json($params);
    }

    public function viewChart()
    {
        return view('students.stats');
    }
}
