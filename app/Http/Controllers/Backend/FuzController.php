<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Student;
use App\Models\City;
use App\Models\District;
use App\Models\School;
use App\Models\Promotion;
use App\Models\Limit;
use Excel;

class FuzController extends Controller
{
    public function indexNew()
    {
        return view('recommendation.indexnew');
    }

    public function query(Request $request)
    {
        $optSekolah = $request->optSekolah;
        $optMahasiswa = $request->optMahasiswa;
        $optSosialisasi = $request->optSosialisasi;
        $optCount1 = $request->optCount1;
        $optCount2 = $request->optCount2;
        $year=$request->year;


        $data = $this->derajatKeanggotaan($year);

        $currentData=[];
        foreach ($data as $key =>$value) {
            if($optCount1=='amp1' && $optCount2=='amp2'){
                $cnt = min($data[$key][$optSekolah], $data[$key][$optMahasiswa], $data[$key][$optSosialisasi]);
            }elseif($optCount1=='amp1' && $optCount2=='sol2'){
                $cnt = min($data[$key][$optSekolah], $data[$key][$optMahasiswa]);
                $cnt = max($cnt, $data[$key][$optSosialisasi]);
            }elseif ($optCount1=='sol1' && $optCount2=='amp2') {
                $cnt = max($data[$key][$optSekolah], $data[$key][$optMahasiswa]);
                $cnt = min($cnt, $data[$key][$optSosialisasi]);
            }else{
                $cnt = max($data[$key][$optSekolah], $data[$key][$optMahasiswa], $data[$key][$optSosialisasi]);
            }
            $currentData[] = [
                'nama_kota' => $data[$key]['nama_kota'],
                'count_sekolah' => $data[$key]['count_sekolah'],
                'count_mahasiswa' => $data[$key]['count_mahasiswa'],
                'count_sosialisasi' => $data[$key]['count_sosialisasi'],
                "$optSekolah" => $data[$key][$optSekolah],
                "$optMahasiswa" => $data[$key][$optMahasiswa],
                "$optSosialisasi" => $data[$key][$optSosialisasi],
                'result_value' => $cnt
            ];
        }

        $collectData=collect($currentData);
        $limitData=$collectData->sortByDesc('result_value')->slice(0,5);
        //dd($limitData);
        $params=[
            'result' => $collectData->sortByDesc('result_value'),
            'value1' => $optSekolah,
            'value2' => $optMahasiswa,
            'value3' => $optSosialisasi,
            'year'=>$year,
            'optCount1'=>$optCount1,
            'optCount2'=>$optCount2,
            'dataMaps' =>$this->dataMaps($year),
            'limitData'=>$limitData
        ];
        //dd($collectData->sortByDesc('result_value'));
        return view('recommendation.table',$params);
    }


    public function  excel(Request $request)
    {
        $optSekolah = $request->optSekolah;
        $optMahasiswa = $request->optMahasiswa;
        $optSosialisasi = $request->optSosialisasi;
        $optCount1 = $request->optCount1;
        $optCount2 = $request->optCount2;
        $year=$request->year;


        $data = $this->derajatKeanggotaan($year);

        $currentData=[];
        foreach ($data as $key =>$value) {
            if($optCount1=='amp1' && $optCount2=='amp2'){
                $cnt = min($data[$key][$optSekolah], $data[$key][$optMahasiswa], $data[$key][$optSosialisasi]);
            }elseif($optCount1=='amp1' && $optCount2=='sol2'){
                $cnt = min($data[$key][$optSekolah], $data[$key][$optMahasiswa]);
                $cnt = max($cnt, $data[$key][$optSosialisasi]);
            }elseif ($optCount1=='sol1' && $optCount2=='amp2') {
                $cnt = max($data[$key][$optSekolah], $data[$key][$optMahasiswa]);
                $cnt = min($cnt, $data[$key][$optSosialisasi]);
            }else{
                $cnt = max($data[$key][$optSekolah], $data[$key][$optMahasiswa], $data[$key][$optSosialisasi]);
            }
            $currentData[] = [
                'nama_kota' => $data[$key]['nama_kota'],
                'count_sekolah' => $data[$key]['count_sekolah'],
                'count_mahasiswa' => $data[$key]['count_mahasiswa'],
                'count_sosialisasi' => $data[$key]['count_sosialisasi'],
                "$optSekolah" => $data[$key][$optSekolah],
                "$optMahasiswa" => $data[$key][$optMahasiswa],
                "$optSosialisasi" => $data[$key][$optSosialisasi],
                'result_value' => $cnt
            ];
        }

        $collectData=collect($currentData);
        $excelData=$collectData->sortByDesc('result_value');
        $params=[
            'data'=>$excelData,
            'title'=>'Hasil Rekomendasi Tahun '.$year,
            'value1' => $optSekolah,
            'value2' => $optMahasiswa,
            'value3' => $optSosialisasi,


        ];
        return Excel::create('hasil_rekomendasi',function ($excel) use($params){
            $excel->sheet('hasil_rekomendasi',function ($sheet) use($params){
                $sheet->loadView('recommendation.excel',$params);
            });
        })->export('xls');
    }






    public function dataMaps($year)
    {
        $dataSekolah = $this->nilaiSekolah($year);
        $dataMahasiswa = $this->nilaiMahasiswa($year);
        $dataSosialisasi = $this->nilaiSosialisasi($year);
        $city = City::all();
        $dataMaps = [];

        foreach ($city as $key => $value) {
            if ($value->id == $dataSekolah[$key]['id_kota'] && $value->id == $dataMahasiswa[$key]['id_kota'] && $value->id == $dataSosialisasi[$key]['id_kota']) {
                $dataMaps[] = [
                    'id_kota' => $value->id,
                    'nama_kota' => $value->nama,
                    'count_sekolah' => $dataSekolah[$key]['count'],
                    'count_mahasiswa' => $dataMahasiswa[$key]['count'],
                    'count_sosialisasi' => $dataSosialisasi[$key]['count']
                ];
            }
        }

        return $dataMaps;
    }

    public function derajatKeanggotaan($year)
    {
        $resultSekolah = $this->nilaiSekolah($year);
        $resultMahasiswa = $this->nilaiMahasiswa($year);
        $resultSosialisasi = $this->nilaiSosialisasi($year);
        $city = City::all();
        $result = [];

        foreach ($city as $key => $value) {
            if ($value->id == $resultSekolah[$key]['id_kota'] && $value->id == $resultMahasiswa[$key]['id_kota'] && $value->id == $resultSosialisasi[$key]['id_kota']) {
                $maxSekolah = $this->maxSekolah($resultSekolah[$key]['cukup'], $resultSekolah[$key]['tinggi'], $resultSekolah[$key]['stinggi']);
                $maxMahasiswa = $this->maxMahasiswa($resultMahasiswa[$key]['srendah'], $resultMahasiswa[$key]['rendah'], $resultMahasiswa[$key]['cukup']);
                $fuzzy = $this->fuzzy($maxSekolah, $maxMahasiswa, $resultSosialisasi[$key]['jarang']);

                $result[] = [
                    'nama_kota' => $value->nama,
                    'count_sekolah' => $resultSekolah[$key]['count'],
                    'srendah_sekolah' => $resultSekolah[$key]['srendah'],
                    'rendah_sekolah' => $resultSekolah[$key]['rendah'],
                    'cukup_sekolah' => $resultSekolah[$key]['cukup'],
                    'tinggi_sekolah' => $resultSekolah[$key]['tinggi'],
                    'stinggi_sekolah' => $resultSekolah[$key]['stinggi'],
                    'count_mahasiswa' => $resultMahasiswa[$key]['count'],
                    'srendah_mahasiswa' => $resultMahasiswa[$key]['srendah'],
                    'rendah_mahasiswa' => $resultMahasiswa[$key]['rendah'],
                    'cukup_mahasiswa' => $resultMahasiswa[$key]['cukup'],
                    'tinggi_mahasiswa' => $resultMahasiswa[$key]['tinggi'],
                    'stinggi_mahasiswa' => $resultMahasiswa[$key]['stinggi'],
                    'count_sosialisasi' => $resultSosialisasi[$key]['count'],
                    'jarang_sosialisasi' => $resultSosialisasi[$key]['jarang'],
                    'sering_sosialisasi' => $resultSosialisasi[$key]['sering'],
                    'max_sekolah' => $maxSekolah,
                    'max_mahasiswa' => $maxMahasiswa,
                    'fuzzy' => $fuzzy
                ];
            }
        }

        return $result;
    }

//	public function index()
//	{
//		$params = [
//            'result' => $this->derajatKeanggotaan()
//        ];
//
//		return view('recommendation.indexnew', $params);
//	}

	public function fuzzy($fz1, $fz2, $fz3)
	{
		$fz = array($fz1, $fz2, $fz3);
		$fuzzy = min($fz);

		return $fuzzy;
	}

	public function nilaiAll($type,$year)
	{
		$city = City::all();
    	$nullData = [];
    	$currData = [];

    	foreach ($city as $key => $value) {
    		if ($type == 1) {
    			$data = School::join('kecamatan', 'sma.id_kecamatan', '=', 'kecamatan.id')->where('kecamatan.id_kota', $value->id)->count();
    		} elseif ($type == 2) {
    			$data = Student::join('kecamatan', 'mahasiswa.id_kecamatan', '=', 'kecamatan.id')->where('kecamatan.id_kota', $value->id)
                    ->where('mahasiswa.angkatan',$year)
                    ->count();
    		} elseif ($type == 3) {
    			$data = Promotion::where('id_kota', $value->id)
                    ->where('ajaran_akhir',$year)
                ->count();
    		}

    		if (is_null($data)) {
    			$nullData[] = [
    				'id_kota' => $value->id,
    				'nama_kota' => $value->nama,
    				'count' => 0
    			];
    		} else {
    			$currData[] = [
    				'id_kota' => $value->id,
    				'nama_kota' => $value->nama,
    				'count' => $data
    			];
    		}
    	}

    	$countData = array_merge($currData, $nullData);

    	return $countData;
	}

	public function maxSekolah($ms1, $ms2, $ms3)
	{
		$ms = array($ms1, $ms2, $ms3);
		$max = max($ms);

		return $max;
	}

    public function nilaiSekolah($year)
    {
    	$derajatSekolah = [];

    	$countData = $this->nilaiAll(1,$year);

    	foreach ($countData as $key => $item) {
    		$derajatSekolah[] = [
    			'id_kota' => $item['id_kota'],
    			'nama_kota' => $item['nama_kota'],
    			'count' => $item['count'],
    			'srendah' => $this->sekolahSRendah($item['count']),
    			'rendah' => $this->sekolahRendah($item['count']),
    			'cukup' => $this->sekolahCukup($item['count']),
    			'tinggi' => $this->sekolahTinggi($item['count']),
    			'stinggi' => $this->sekolahSTinggi($item['count'])
    		];
    	}

    	return $derajatSekolah;
    }

    public function sekolahSRendah($vals)
    {
    	$limit = Limit::all();

    	foreach ($limit as $key => $value) {
    		if (($value->variabel == 'sma') && ($value->himpunan == 'srendah') && ($value->jenis == 'tturun')) {
    			if ($vals <= $value->tengah) {
    				$result = 1;
    			} elseif ($vals >= $value->tengah && $vals <= $value->atas) {
    				$result = ($value->atas - $vals)/($value->atas - $value->tengah);
    			} elseif ($vals >= $value->atas) {
    				$result = 0;
    			}
    		}
    	}

    	return $result;
    }

    public function sekolahRendah($vals)
    {
    	$limit = Limit::all();

    	foreach ($limit as $key => $value) {
    		if (($value->variabel == 'sma') && ($value->himpunan == 'rendah') && ($value->jenis == 'segitiga')) {
    			if ($vals <= $value->bawah || $vals >= $value->atas) {
    				$result = 0;
    			} elseif ($vals >= $value->bawah && $vals <= $value->tengah) {
    				$result = ($vals - $value->bawah)/($value->tengah - $value->bawah);
    			} elseif ($vals >= $value->tengah && $vals <= $value->atas) {
    				$result = ($value->atas - $vals)/($value->atas - $value->tengah);
    			}
    		}
    	}

    	return $result;
    }

    public function sekolahCukup($vals)
    {
    	$limit = Limit::all();

    	foreach ($limit as $key => $value) {
    		if (($value->variabel == 'sma') && ($value->himpunan == 'cukup') && ($value->jenis == 'segitiga')) {
    			if ($vals <= $value->bawah || $vals >= $value->atas) {
    				$result = 0;
    			} elseif ($vals >= $value->bawah && $vals <= $value->tengah) {
    				$result = ($vals - $value->bawah)/($value->tengah - $value->bawah);
    			} elseif ($vals >= $value->tengah && $vals <= $value->atas) {
    				$result = ($value->atas - $vals)/($value->atas - $value->tengah);
    			}
    		}
    	}

    	return $result;
    }

    public function sekolahTinggi($vals)
    {
    	$limit = Limit::all();

    	foreach ($limit as $key => $value) {
    		if (($value->variabel == 'sma') && ($value->himpunan == 'tinggi') && ($value->jenis == 'segitiga')) {
    			if ($vals <= $value->bawah || $vals >= $value->atas) {
    				$result = 0;
    			} elseif ($vals >= $value->bawah && $vals <= $value->tengah) {
    				$result = ($vals - $value->bawah)/($value->tengah - $value->bawah);
    			} elseif ($vals >= $value->tengah && $vals <= $value->atas) {
    				$result = ($value->atas - $vals)/($value->atas - $value->tengah);
    			}
    		}
    	}

    	return $result;
    }

    public function sekolahSTinggi($vals)
    {
    	$limit = Limit::all();

    	foreach ($limit as $key => $value) {
    		if (($value->variabel == 'sma') && ($value->himpunan == 'stinggi') && ($value->jenis == 'tnaik')) {
    			if ($vals <= $value->bawah) {
    				$result = 0;
    			} elseif ($vals >= $value->bawah && $vals <= $value->tengah) {
    				$result = ($vals - $value->bawah)/($value->tengah - $value->bawah);
    			} elseif ($vals >= $value->tengah) {
    				$result = 1;
    			}
    		}
    	}

    	return $result;
    }

    public function maxMahasiswa($mm1, $mm2, $mm3)
	{
		$mm = array($mm1, $mm2, $mm3);
		$max = max($mm);

		return $max;
	}

    public function nilaiMahasiswa($year)
    {
    	$derajatMahasiswa = [];

    	$countData = $this->nilaiAll(2,$year);

    	foreach ($countData as $key => $item) {
    		$derajatMahasiswa[] = [
    			'id_kota' => $item['id_kota'],
    			'nama_kota' => $item['nama_kota'],
    			'count' => $item['count'],
    			'srendah' => $this->mahasiswaSrendah($item['count']),
    			'rendah' => $this->mahasiswarendah($item['count']),
    			'cukup' => $this->mahasiswaCukup($item['count']),
    			'tinggi' => $this->mahasiswatinggi($item['count']),
    			'stinggi' => $this->mahasiswaStinggi($item['count'])
    		];
    	}

    	return $derajatMahasiswa;
    }

    public function mahasiswaSRendah($v)
    {
    	$limit = Limit::all();

    	foreach ($limit as $key => $value) {
    		if (($value->variabel == 'mahasiswa') && ($value->himpunan == 'srendah') && ($value->jenis == 'tturun')) {
    			if ($v <= $value->tengah) {
    				$result = 1;
    			} elseif ($v >= $value->tengah && $v <= $value->atas) {
    				$result = ($value->atas - $v)/($value->atas - $value->tengah);
    			} elseif ($v >= $value->atas) {
    				$result = 0;
    			}
    		}
    	}

    	return $result;
    }

    public function mahasiswaRendah($v)
    {
    	$limit = Limit::all();

    	foreach ($limit as $key => $value) {
    		if (($value->variabel == 'mahasiswa') && ($value->himpunan == 'rendah') && ($value->jenis == 'segitiga')) {
    			if ($v <= $value->bawah || $v >= $value->atas) {
    				$result = 0;
    			} elseif ($v >= $value->bawah && $v <= $value->tengah) {
    				$result = ($v - $value->bawah)/($value->tengah - $value->bawah);
    			} elseif ($v >= $value->tengah && $v <= $value->atas) {
    				$result = ($value->atas - $v)/($value->atas - $value->tengah);
    			}
    		}
    	}

    	return $result;
    }

    public function mahasiswaCukup($v)
    {
    	$limit = Limit::all();

    	foreach ($limit as $key => $value) {
    		if (($value->variabel == 'mahasiswa') && ($value->himpunan == 'cukup') && ($value->jenis == 'segitiga')) {
    			if ($v <= $value->bawah || $v >= $value->atas) {
    				$result = 0;
    			} elseif ($v >= $value->bawah && $v <= $value->tengah) {
    				$result = ($v - $value->bawah)/($value->tengah - $value->bawah);
    			} elseif ($v >= $value->tengah && $v <= $value->atas) {
    				$result = ($value->atas - $v)/($value->atas - $value->tengah);
    			}
    		}
    	}

    	return $result;
    }

    public function mahasiswaTinggi($v)
    {
    	$limit = Limit::all();

    	foreach ($limit as $key => $value) {
    		if (($value->variabel == 'mahasiswa') && ($value->himpunan == 'tinggi') && ($value->jenis == 'segitiga')) {
    			if ($v <= $value->bawah || $v >= $value->atas) {
    				$result = 0;
    			} elseif ($v >= $value->bawah && $v <= $value->tengah) {
    				$result = ($v - $value->bawah)/($value->tengah - $value->bawah);
    			} elseif ($v >= $value->tengah && $v <= $value->atas) {
    				$result = ($value->atas - $v)/($value->atas - $value->tengah);
    			}
    		}
    	}

    	return $result;
    }

    public function mahasiswaSTinggi($v)
    {
    	$limit = Limit::all();

    	foreach ($limit as $key => $value) {
    		if (($value->variabel == 'mahasiswa') && ($value->himpunan == 'stinggi') && ($value->jenis == 'tnaik')) {
    			if ($v <= $value->bawah) {
    				$result = 0;
    			} elseif ($v >= $value->bawah && $v <= $value->tengah) {
    				$result = ($v - $value->bawah)/($value->tengah - $value->bawah);
    			} elseif ($v >= $value->tengah) {
    				$result = 1;
    			}
    		}
    	}

    	return $result;
    }

    public function nilaiSosialisasi($year)
    {
    	$derajatSosialisasi = [];

    	$countData = $this->nilaiAll(3,$year);

    	foreach ($countData as $key => $item) {
    		$derajatSosialisasi[] = [
    			'id_kota' => $item['id_kota'],
    			'nama_kota' => $item['nama_kota'],
    			'count' => $item['count'],
    			'jarang' => $this->sosialisasiJarang($item['count']),
    			'sering' => $this->sosialisasiSering($item['count'])
    		];
    	}

    	return $derajatSosialisasi;
    }

    public function sosialisasiJarang($val)
    {
    	$limit = Limit::all();

    	foreach ($limit as $key => $value) {
    		if (($value->variabel == 'sosialisasi') && ($value->himpunan == 'jarang') && ($value->jenis == 'tturun')) {
    			if ($val <= $value->tengah) {
	    			$result = 1;
	    		} elseif ($val >= $value->tengah && $val <= $value->atas) {
	    			$result = ($value->atas - $val)/($value->atas - $value->tengah);
	    		} elseif ($val >= $value->atas) {
	    			$result = 0;
	    		}
    		}
    	}

    	return $result;
    }

    public function sosialisasiSering($val)
    {
    	$limit = Limit::all();

    	foreach ($limit as $key => $value) {
    		if (($value->variabel == 'sosialisasi') && ($value->himpunan == 'sering') && ($value->jenis == 'tnaik')) {
    			if ($val <= $value->bawah) {
    				$result = 0;
    			} elseif ($val >= $value->bawah && $val <= $value->tengah) {
    				$result = ($val - $value->bawah)/($value->tengah - $value->bawah);
    			} elseif ($val >= $value->tengah) {
    				$result = 1;
    			}
    		}
    	}

    	return $result;
    }
}
