<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Student;
use App\Models\City;
use App\Models\District;
use App\Models\School;
use App\Models\Promotion;
use App\Models\Limit;

class FuzzyController extends Controller
{
	public function index()
	{
		$resultSekolah = $this->nilaiSekolah();
		$resultMahasiswa = $this->nilaiMahasiswa();
		$resultSosialisasi = $this->nilaiSosialisasi();
		$city = City::all();
		$result = [];

		foreach ($city as $key => $value) {
			if ($value->id == $resultSekolah[$key]['id_kota'] && $value->id == $resultMahasiswa[$key]['id_kota'] && $value->id == $resultSosialisasi[$key]['id_kota']) {
				$maxSekolah = $this->maxSekolah($resultSekolah[$key]['cukup'], $resultSekolah[$key]['tinggi'], $resultSekolah[$key]['stinggi']);
				$maxMahasiswa = $this->maxMahasiswa($resultMahasiswa[$key]['srendah'], $resultMahasiswa[$key]['rendah'], $resultMahasiswa[$key]['cukup']);
				$fuzzy = $this->fuzzy($maxSekolah, $maxMahasiswa, $resultSosialisasi[$key]['jarang']);

				$result[] = [
					'nama_kota' => $value->nama,
					'count_sekolah' => $resultSekolah[$key]['count'],
					'srendah_sekolah' => $resultSekolah[$key]['srendah'],
					'rendah_sekolah' => $resultSekolah[$key]['rendah'],
					'cukup_sekolah' => $resultSekolah[$key]['cukup'],
					'tinggi_sekolah' => $resultSekolah[$key]['tinggi'],
					'stinggi_sekolah' => $resultSekolah[$key]['stinggi'],
					'count_mahasiswa' => $resultMahasiswa[$key]['count'],
					'srendah_mahasiswa' => $resultMahasiswa[$key]['srendah'],
					'rendah_mahasiswa' => $resultMahasiswa[$key]['rendah'],
					'cukup_mahasiswa' => $resultMahasiswa[$key]['cukup'],
					'tinggi_mahasiswa' => $resultMahasiswa[$key]['tinggi'],
					'stinggi_mahasiswa' => $resultMahasiswa[$key]['stinggi'],
					'count_sosialisasi' => $resultSosialisasi[$key]['count'],
					'jarang_sosialisasi' => $resultSosialisasi[$key]['jarang'],
					'sering_sosialisasi' => $resultSosialisasi[$key]['sering'],
					'max_sekolah' => $maxSekolah,
					'max_mahasiswa' => $maxMahasiswa,
					'fuzzy' => $fuzzy
				];
			}
		}

		$params = [
			'result' => $result
		];

		// dd($params);
		return view('recommendation.index', $params);
	}

    public function dataMaps()
    {
        $dataSekolah = $this->nilaiSekolah();
        $dataMahasiswa = $this->nilaiMahasiswa();
        $dataSosialisasi = $this->nilaiSosialisasi();
        $city = City::all();
        $dataMaps = [];

        foreach ($city as $key => $value) {
            if ($value->id == $dataSekolah[$key]['id_kota'] && $value->id == $dataMahasiswa[$key]['id_kota'] && $value->id == $dataSosialisasi[$key]['id_kota']) {
                $dataMaps[] = [
                    'id_kota' => $value->id,
                    'nama_kota' => $value->nama,
                    'count_sekolah' => $dataSekolah[$key]['count'],
                    'count_mahasiswa' => $dataMahasiswa[$key]['count'],
                    'count_sosialisasi' => $dataSosialisasi[$key]['count']
                ];
            }
        }

        $params = [
            'data_maps' => $dataMaps
        ];

        return response()->json($params);
    }

	public function fuzzy($fz1, $fz2, $fz3)
	{
		$fz = array($fz1, $fz2, $fz3);
		$fuzzy = min($fz);

		return $fuzzy;
	}

	public function nilaiAll($type)
	{
		$city = City::all();
    	$nullData = [];
    	$currData = [];

    	foreach ($city as $key => $value) {
    		if ($type == 1) {
    			$data = School::join('kecamatan', 'sma.id_kecamatan', '=', 'kecamatan.id')->where('kecamatan.id_kota', $value->id)->count();
    		} elseif ($type == 2) {
    			$data = Student::join('kecamatan', 'mahasiswa.id_kecamatan', '=', 'kecamatan.id')->where('kecamatan.id_kota', $value->id)->count();
    		} elseif ($type == 3) {
    			$data = Promotion::where('id_kota', $value->id)->count();
    		}

    		if (is_null($data)) {
    			$nullData[] = [
    				'id_kota' => $value->id,
    				'nama_kota' => $value->nama,
    				'count' => 0
    			];
    		} else {
    			$currData[] = [
    				'id_kota' => $value->id,
    				'nama_kota' => $value->nama,
    				'count' => $data
    			];
    		}
    	}

    	$countData = array_merge($currData, $nullData);

    	return $countData;
	}

	public function maxSekolah($ms1, $ms2, $ms3)
	{
		$ms = array($ms1, $ms2, $ms3);
		$max = max($ms);

		return $max;
	}

    public function nilaiSekolah()
    {
    	// $query_sekolah = School::selectRaw('kota.nama, count(*) as jumlah_sekolah')
    	// 							->join('kecamatan', 'kecamatan.id', '=', 'sma.id_kecamatan')
    	// 							->join('kota', 'kota.id', '=', 'kecamatan.id_kota')
    	// 							->groupBy('kota.nama')
    	// 							->get();
    	// $params = [
    	// 	'query_sekolah' => $query_sekolah
    	// ];

    	// return view('recommendation.index', $params);


    	// $city = City::all();
    	// $nullData = [];
    	// $currData = [];
    	$derajatSekolah = [];
    	// foreach ($city as $key => $value) {
    	// 	$data = School::join('kecamatan', 'sma.id_kecamatan', '=', 'kecamatan.id')->where('kecamatan.id_kota', $value->id)->count();
    	// 	if (is_null($data)) {
    	// 		$nullData[] = [
    	// 			'nama_kota' => $value->nama,
    	// 			'count' => 0
    	// 		];
    	// 	} else {
    	// 		$currData[] = [
    	// 			'nama_kota' => $value->nama,
    	// 			'count' => $data
    	// 		];
    	// 	}
    	// }

    	$countData = $this->nilaiAll(1);

    	foreach ($countData as $key => $item) {
    		$derajatSekolah[] = [
    			'id_kota' => $item['id_kota'],
    			'nama_kota' => $item['nama_kota'],
    			'count' => $item['count'],
    			'srendah' => $this->sekolahSRendah($item['count']),
    			'rendah' => $this->sekolahRendah($item['count']),
    			'cukup' => $this->sekolahCukup($item['count']),
    			'tinggi' => $this->sekolahTinggi($item['count']),
    			'stinggi' => $this->sekolahSTinggi($item['count'])
    		];
    	}

    	return $derajatSekolah;
    }

    public function sekolahSRendah($vals)
    {
    	$limit = Limit::all();

    	foreach ($limit as $key => $value) {
    		if (($value->variabel == 'sma') && ($value->himpunan == 'srendah') && ($value->jenis == 'tturun')) {
    			if ($vals <= $value->tengah) {
    				$result = 1;
    			} elseif ($vals >= $value->tengah && $vals <= $value->atas) {
    				$result = ($value->atas - $vals)/($value->atas - $value->tengah);
    			} elseif ($vals >= $value->atas) {
    				$result = 0;
    			}
    		}
    	}

    	return $result;
    }

    public function sekolahRendah($vals)
    {
    	$limit = Limit::all();

    	foreach ($limit as $key => $value) {
    		if (($value->variabel == 'sma') && ($value->himpunan == 'rendah') && ($value->jenis == 'segitiga')) {
    			if ($vals <= $value->bawah || $vals >= $value->atas) {
    				$result = 0;
    			} elseif ($vals >= $value->bawah && $vals <= $value->tengah) {
    				$result = ($vals - $value->bawah)/($value->tengah - $value->bawah);
    			} elseif ($vals >= $value->tengah && $vals <= $value->atas) {
    				$result = ($value->atas - $vals)/($value->atas - $value->tengah);
    			}
    		}
    	}

    	return $result;
    }

    public function sekolahCukup($vals)
    {
    	$limit = Limit::all();

    	foreach ($limit as $key => $value) {
    		if (($value->variabel == 'sma') && ($value->himpunan == 'cukup') && ($value->jenis == 'segitiga')) {
    			if ($vals <= $value->bawah || $vals >= $value->atas) {
    				$result = 0;
    			} elseif ($vals >= $value->bawah && $vals <= $value->tengah) {
    				$result = ($vals - $value->bawah)/($value->tengah - $value->bawah);
    			} elseif ($vals >= $value->tengah && $vals <= $value->atas) {
    				$result = ($value->atas - $vals)/($value->atas - $value->tengah);
    			}
    		}
    	}

    	return $result;
    }

    public function sekolahTinggi($vals)
    {
    	$limit = Limit::all();

    	foreach ($limit as $key => $value) {
    		if (($value->variabel == 'sma') && ($value->himpunan == 'tinggi') && ($value->jenis == 'segitiga')) {
    			if ($vals <= $value->bawah || $vals >= $value->atas) {
    				$result = 0;
    			} elseif ($vals >= $value->bawah && $vals <= $value->tengah) {
    				$result = ($vals - $value->bawah)/($value->tengah - $value->bawah);
    			} elseif ($vals >= $value->tengah && $vals <= $value->atas) {
    				$result = ($value->atas - $vals)/($value->atas - $value->tengah);
    			}
    		}
    	}

    	return $result;
    }

    public function sekolahSTinggi($vals)
    {
    	$limit = Limit::all();

    	foreach ($limit as $key => $value) {
    		if (($value->variabel == 'sma') && ($value->himpunan == 'stinggi') && ($value->jenis == 'tnaik')) {
    			if ($vals <= $value->bawah) {
    				$result = 0;
    			} elseif ($vals >= $value->bawah && $vals <= $value->tengah) {
    				$result = ($vals - $value->bawah)/($value->tengah - $value->bawah);
    			} elseif ($vals >= $value->tengah) {
    				$result = 1;
    			}
    		}
    	}

    	return $result;
    }

    public function maxMahasiswa($mm1, $mm2, $mm3)
	{
		$mm = array($mm1, $mm2, $mm3);
		$max = max($mm);

		return $max;
	}

    public function nilaiMahasiswa()
    {
    	// $query_mahasiswa = Student::selectRaw('kota.nama, count(*) as jumlah_mahasiswa')
    	// 								->join('kecamatan', 'kecamatan.id', '=', 'mahasiswa.id_kecamatan')
	    // 								->join('kota', 'kota.id', '=', 'kecamatan.id_kota')
	    // 								->groupBy('kota.nama')
	    // 								->get();
	    // $params = [
    	// 	'query_mahasiswa' => $query_mahasiswa
    	// ];

    	// return view('recommendation.index', $params);

    	// $city = City::all();
    	// $nullData = [];
    	// $currData = [];
    	$derajatMahasiswa = [];
    	// foreach ($city as $key => $value) {
    	// 	$data = Student::join('kecamatan', 'mahasiswa.id_kecamatan', '=', 'kecamatan.id')->where('kecamatan.id_kota', $value->id)->count();
    	// 	if (is_null($data)) {
    	// 		$nullData[] = [
    	// 			'nama_kota' => $value->nama,
    	// 			'count' => 0
    	// 		];
    	// 	} else {
    	// 		$currData[] = [
    	// 			'nama_kota' => $value->nama,
    	// 			'count' => $data
    	// 		];
    	// 	}
    	// }

    	$countData = $this->nilaiAll(2);

    	foreach ($countData as $key => $item) {
    		$derajatMahasiswa[] = [
    			'id_kota' => $item['id_kota'],
    			'nama_kota' => $item['nama_kota'],
    			'count' => $item['count'],
    			'srendah' => $this->mahasiswaSrendah($item['count']),
    			'rendah' => $this->mahasiswarendah($item['count']),
    			'cukup' => $this->mahasiswaCukup($item['count']),
    			'tinggi' => $this->mahasiswatinggi($item['count']),
    			'stinggi' => $this->mahasiswaStinggi($item['count'])
    		];
    	}

    	return $derajatMahasiswa;
    }

    public function mahasiswaSRendah($v)
    {
    	$limit = Limit::all();

    	foreach ($limit as $key => $value) {
    		if (($value->variabel == 'mahasiswa') && ($value->himpunan == 'srendah') && ($value->jenis == 'tturun')) {
    			if ($v <= $value->tengah) {
    				$result = 1;
    			} elseif ($v >= $value->tengah && $v <= $value->atas) {
    				$result = ($value->atas - $v)/($value->atas - $value->tengah);
    			} elseif ($v >= $value->atas) {
    				$result = 0;
    			}
    		}
    	}

    	return $result;
    }

    public function mahasiswaRendah($v)
    {
    	$limit = Limit::all();

    	foreach ($limit as $key => $value) {
    		if (($value->variabel == 'mahasiswa') && ($value->himpunan == 'rendah') && ($value->jenis == 'segitiga')) {
    			if ($v <= $value->bawah || $v >= $value->atas) {
    				$result = 0;
    			} elseif ($v >= $value->bawah && $v <= $value->tengah) {
    				$result = ($v - $value->bawah)/($value->tengah - $value->bawah);
    			} elseif ($v >= $value->tengah && $v <= $value->atas) {
    				$result = ($value->atas - $v)/($value->atas - $value->tengah);
    			}
    		}
    	}

    	return $result;
    }

    public function mahasiswaCukup($v)
    {
    	$limit = Limit::all();

    	foreach ($limit as $key => $value) {
    		if (($value->variabel == 'mahasiswa') && ($value->himpunan == 'cukup') && ($value->jenis == 'segitiga')) {
    			if ($v <= $value->bawah || $v >= $value->atas) {
    				$result = 0;
    			} elseif ($v >= $value->bawah && $v <= $value->tengah) {
    				$result = ($v - $value->bawah)/($value->tengah - $value->bawah);
    			} elseif ($v >= $value->tengah && $v <= $value->atas) {
    				$result = ($value->atas - $v)/($value->atas - $value->tengah);
    			}
    		}
    	}

    	return $result;
    }

    public function mahasiswaTinggi($v)
    {
    	$limit = Limit::all();

    	foreach ($limit as $key => $value) {
    		if (($value->variabel == 'mahasiswa') && ($value->himpunan == 'tinggi') && ($value->jenis == 'segitiga')) {
    			if ($v <= $value->bawah || $v >= $value->atas) {
    				$result = 0;
    			} elseif ($v >= $value->bawah && $v <= $value->tengah) {
    				$result = ($v - $value->bawah)/($value->tengah - $value->bawah);
    			} elseif ($v >= $value->tengah && $v <= $value->atas) {
    				$result = ($value->atas - $v)/($value->atas - $value->tengah);
    			}
    		}
    	}

    	return $result;
    }

    public function mahasiswaSTinggi($v)
    {
    	$limit = Limit::all();

    	foreach ($limit as $key => $value) {
    		if (($value->variabel == 'mahasiswa') && ($value->himpunan == 'stinggi') && ($value->jenis == 'tnaik')) {
    			if ($v <= $value->bawah) {
    				$result = 0;
    			} elseif ($v >= $value->bawah && $v <= $value->tengah) {
    				$result = ($v - $value->bawah)/($value->tengah - $value->bawah);
    			} elseif ($v >= $value->tengah) {
    				$result = 1;
    			}
    		}
    	}

    	return $result;
    }

    public function nilaiSosialisasi()
    {
    	// $query_sosialisasi = Promotion::selectRaw('kota.nama, count(*) as jumlah_sosialisasi')
	    // 								->join('kota', 'kota.id', '=', 'sosialisasi.id_kota')
	    // 								->groupBy('kota.nama')
	    // 								->get();

	    // $query_sosialisasi = City::selectRaw('kota.nama, count(*) as jumlah_sosialisasi')
	    // 								->leftJoin('sosialisasi', 'sosialisasi.id_kota', '=', 'kota.id')
	    // 								->groupBy('kota.nama')
	    // 								->get();
	    // $params = [
    	// 	'query_sosialisasi' => $query_sosialisasi
    	// ];

    	// return view('recommendation.index', $params);


    	// $city = City::all();
    	// $nullData = [];
    	// $currData = [];
    	$derajatSosialisasi = [];
    	// foreach ($city as $key => $value) {
    	// 	$data = Promotion::where('id_kota', $value->id)->count();
    	// 	if (is_null($data)) {
    	// 		$nullData[] = [
    	// 			'nama_kota' => $value->nama,
    	// 			'count' => 0
    	// 		];
    	// 	} else {
    	// 		$currData[] = [
    	// 			'nama_kota' => $value->nama,
    	// 			'count' => $data
    	// 		];
    	// 	}
    	// }

    	$countData = $this->nilaiAll(3);

    	foreach ($countData as $key => $item) {
    		$derajatSosialisasi[] = [
    			'id_kota' => $item['id_kota'],
    			'nama_kota' => $item['nama_kota'],
    			'count' => $item['count'],
    			'jarang' => $this->sosialisasiJarang($item['count']),
    			'sering' => $this->sosialisasiSering($item['count'])
    		];
    	}

    	return $derajatSosialisasi;
    }

    public function sosialisasiJarang($val)
    {
    	$limit = Limit::all();

    	foreach ($limit as $key => $value) {
    		if (($value->variabel == 'sosialisasi') && ($value->himpunan == 'jarang') && ($value->jenis == 'tturun')) {
    			if ($val <= $value->tengah) {
	    			$result = 1;
	    		} elseif ($val >= $value->tengah && $val <= $value->atas) {
	    			$result = ($value->atas - $val)/($value->atas - $value->tengah);
	    		} elseif ($val >= $value->atas) {
	    			$result = 0;
	    		}
    		}
    	}

    	return $result;
    }

    public function sosialisasiSering($val)
    {
    	$limit = Limit::all();

    	foreach ($limit as $key => $value) {
    		if (($value->variabel == 'sosialisasi') && ($value->himpunan == 'sering') && ($value->jenis == 'tnaik')) {
    			if ($val <= $value->bawah) {
    				$result = 0;
    			} elseif ($val >= $value->bawah && $val <= $value->tengah) {
    				$result = ($val - $value->bawah)/($value->tengah - $value->bawah);
    			} elseif ($val >= $value->tengah) {
    				$result = 1;
    			}
    		}
    	}

    	return $result;
    }
}
