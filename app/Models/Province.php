<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $table = 'provinsi';
    protected $primaryKey = 'id';
    protected $fillable = [
    	'id', 'nama'
    ];
}
