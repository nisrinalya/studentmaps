<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Promotion extends Model
{
    protected $table = 'sosialisasi';
    protected $primaryKey = 'id';
    protected $fillable = [
    	'nama', 'tanggal', 'alamat', 'id_kota', 'ajaran_awal', 'ajaran_akhir'
    ];

    public function getKota()
    {
    	return $this->hasOne('App\Models\City', 'id', 'id_kota');
    }
}
