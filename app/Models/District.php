<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    protected $table = 'kecamatan';
    protected $primaryKey = 'id';
    protected $fillable = [
    	'id', 'nama', 'longitude', 'latitude', 'id_kota'
    ];

    public function getKota()
    {
    	return $this->hasOne('App\Models\City', 'id', 'id_kota');
    }
}
