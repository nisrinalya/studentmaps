<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = 'kota';
    protected $primaryKey = 'id';
    protected $fillable = [
    	'id', 'nama', 'longitude', 'latitude', 'id_provinsi'
    ];

    public function getProvinsi()
    {
    	return $this->hasOne('App\Models\Province', 'id', 'id_provinsi');
    }
}
