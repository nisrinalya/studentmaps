<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $table = 'mahasiswa';
    protected $primaryKey = 'id';
    protected $fillable = [
    	'nrp', 'nama', 'program_studi', 'jenis_kelamin', 'alamat', 'id_kecamatan', 'id_sma', 'jalur_penerimaan'
    ];

    public function getKecamatan()
    {
    	return $this->hasOne('App\Models\District', 'id', 'id_kecamatan');
    }

    public function getSekolah()
    {
    	return $this->hasOne('App\Models\School', 'id', 'id_sma');
    }
}
