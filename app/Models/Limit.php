<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Limit extends Model
{
    protected $table = 'batas';
    protected $fillable = [
    	'variabel', 'himpunan', 'jenis', 'bawah', 'tengah', 'atas'
    ];
}
