<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    protected $table = 'sma';
    protected $primaryKey = 'id';
    protected $fillable = [
    	'nama', 'npsn', 'status', 'kategori', 'alamat', 'id_kecamatan'
    ];

    public function getKecamatan()
    {
    	return $this->hasOne('App\Models\District', 'id', 'id_kecamatan');
    }
}
