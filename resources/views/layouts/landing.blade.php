<!DOCTYPE html>
<html lang="zxx" class="no-js">

@include('frontpartials._head')

<body>
	<!-- ================ Start Header Area ================= -->
    <header class="default-header">
        <nav class="navbar navbar-expand-lg  navbar-light">
            <div class="container">
                <a class="navbar-brand" href="index.html">
                    <img src="{{asset('public/assets/frontend/img/logo_pens_polos.png')}}" alt="" style="width: 75px" />
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="lnr lnr-menu"></span>
                </button>

                <div class="collapse navbar-collapse justify-content-end align-items-center" id="navbarSupportedContent">
                    <ul class="navbar-nav">
                        <li style="padding-top: 5px;"><a href="{{ url('frontend') }}">Beranda</a></li>
                        <li class="dropdown" style="padding-top: 5px;">
                            <a class="dropdown-toggle" href="#" data-toggle="dropdown">Statistik</a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="{{ url('frontend/school-stats') }}">Statistik Jumlah SMA/SMK Sederajat</a>
                                <a class="dropdown-item" href="{{ url('frontend/student-stats') }}">Statistik Jumlah Mahasiswa</a>
                                <a class="dropdown-item" href="{{ url('frontend/promotion-stats') }}">Statistik Jumlah Riwayat Sosialisasi</a>
                            </div>
                        </li>
                        <li style="padding-top: 5px;"><a href="{{ url('frontend/maps') }}">Peta Sebaran</a></li>
                        <li><a href="{{ url('login') }}" class="genric-btn info-border circle small" style="height: 30px; padding-top: 3px;">Masuk Sebagai Admin</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
    <!-- ================ End Header Area ================= -->

    <!-- ================ contents start ================ -->
    @yield('frontcontent')
    <!-- ================ contents end ================ -->

    @include('frontpartials._script')

</body>
</html>