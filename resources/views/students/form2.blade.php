@extends('layouts.master')
@section('title', 'Student Form')
@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>STUDENT FORM</h2>
            </div>

            <!-- Horizontal Layout -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>STUDENT FORM</h2>
                        </div>
                        <div class="body">
                            <form class="form-horizontal">
                            	<div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="nrp">NRP</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="nrp" class="form-control" placeholder="Enter NRP here">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="nama">Name</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="nama" class="form-control" placeholder="Enter student name here">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="program_studi">Major</label>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <div class="form-group">
                                            <select class="form-control show-tick" data-live-search="true" name="program_studi" id="program_studi">
                                                <option>- Select Major -</option>
                                                <option value="D3-Multimedia Broadcasting">D3 MULTIMEDIA BROADCASTING</option>
                                                <option value="D3-Teknik Elektro Industri">D3 TEKNIK ELEKTRO INDUSTRI</option>
                                                <option value="D3-Teknik Elektronika">D3 TEKNIK ELEKTRONIKA</option>
                                                <option value="D3-Teknik Informatika">D3 TEKNIK INFORMATIKA</option>
                                                <option value="D3-Teknik Telekomunikasi">D3 TEKNIK TELEKOMUNIKASI</option>
                                                <option value="D4-Sistem Pembangkit Energi">D4 SISTEM PEMBANGKIT ENERGI</option>
                                                <option value="D4-Teknik Komputer">D4 TEKNIK KOMPUTER</option>
                                                <option value="D4-Teknik Mekatronika">D4 TEKNIK MEKATRONIKA</option>
                                                <option value="D4-Teknologi Game">D4 TEKNOLOGI GAME</option>
                                                <option value="D4-Teknik Elektro Industri">D4 TEKNIK ELEKTRO INDUSTRI</option>
                                                <option value="D4-Teknik Elektronika">D4 TEKNIK ELEKTRONIKA</option>
                                                <option value="D4-Teknik Informatika">D4 TEKNIK INFORMATIKA</option>
                                                <option value="D4-Teknik Telekomunikasi">D4 TEKNIK TELEKOMUNIKASI</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="jenis_kelamin">Gender</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
		                                    <input type="radio" name="jenis_kelamin" class="radio-col-amber" value="L" id="laki">
		                                    <label for="laki">Male</label>

		                                    <input type="radio" name="jenis_kelamin" class="radio-col-amber" value="L" id="perempuan">
		                                    <label for="perempuan" class="m-l-20">Female</label>
		                                </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="alamat">Address</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="alamat" class="form-control" placeholder="Enter student address here">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="id_kota">City</label>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                        <div class="form-group">
                                            <select class="form-control show-tick" data-live-search="true" name="id_kota" id="id_kota">
                                                <option>- Select City -</option>

                                                @foreach($kota as $num => $item)
                                                    @if(isset($kota->nama))
                                                        @if($kota->id==$item->id)
                                                            <option value="{{ $item->id }}" selected="selected">{{ $item->nama }}</option>
                                                        @else
                                                            <option value="{{ $item->id }}">{{ $item->nama }}</option>
                                                        @endif
                                                    @else
                                                        <option value="{{ $item->id }}">{{ $item->nama }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="id_kecamatan">District</label>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                        <div class="form-group">
                                            <select class="form-control show-tick" data-live-search="true" name="id_kecamatan" id="id_kecamatan">
                                                <option>- Select District -</option>
                                                <!-- @foreach($kecamatan as $num => $item)
                                                    @if(isset($kecamatan->nama))
                                                        @if($kecamatan->id==$item->id)
                                                            <option value="{{ $item->id }}" selected="selected">{{ $item->nama }}</option>
                                                        @else
                                                            <option value="{{ $item->id }}">{{ $item->nama }}</option>
                                                        @endif
                                                    @else
                                                        <option value="{{ $item->id }}">{{ $item->nama }}</option>
                                                    @endif
                                                @endforeach -->
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="sma">High School</label>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <div class="form-group">
                                            <select class="form-control show-tick" data-live-search="true" name="sma" id="sma">
                                                <option>- Select High School -</option>
                                                <!-- @foreach($sekolah as $num => $item)
                                                    @if(isset($sekolah->nama))
                                                        @if($sekolah->id==$item->id)
                                                            <option value="{{ $item->id }}" selected="selected">{{ $item->nama }}</option>
                                                        @else
                                                            <option value="{{ $item->id }}">{{ $item->nama }}</option>
                                                        @endif
                                                    @else
                                                        <option value="{{ $item->id }}">{{ $item->nama }}</option>
                                                    @endif
                                                @endforeach -->
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="jalur_penerimaan">Admission Track</label>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                        <div class="form-group">
                                            <select class="form-control show-tick" data-live-search="true" name="jalur_penerimaan" id="jalur_penerimaan">
                                                <option>- Select Admission Track -</option>
                                                <option value="Bidik Misi">BIDIK MISI</option>
                                                <option value="PMDK Berprestasi">PMDK BERPRESTASI</option>
                                                <option value="SIMANDIRI">SIMANDIRI</option>
                                                <option value="UMPN">UMPN</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
                                        <button type="button" class="btn btn-success m-t-15 waves-effect">ADD DATA</button>
                                        <button type="button" class="btn btn-danger m-t-15 waves-effect">CANCEL</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Horizontal Layout -->
        </div>
    </section>



@endsection

@section('scripts')
<script>
        // function getKecamatan() {
        //     $('#id_kota').on('change', function (e) {
        //         $('#id_kecamatan').find('option').remove().end();

        //         var id = $('#id_kota option:selected').attr('value');
        //         var info = $.get("{{ url('student/get-option') }}", {id_kota:id});

        //         info.done(function (data) {
        //            console.log(data);

        //            $.each(data, function (index, subcatObj) {
        //               $('#id_kecamatan').append('<option value="'+subcatObj.id+'">'+subcatObj.nama+'</option>');
        //            });
        //         });

        //         info.fail(function () {
        //             console.log(e);
        //         });
        //     });
        // }

        // $(document).ready(function () {
        //     getKecamatan();
        // });

            $('select[name="id_kota"]').on('change', function() {
                var stateID = $(this).val();
                if(stateID) {
                    $.ajax({
                        url: "{{ url('/student/getdistrict') }}" + '/' + stateID,
                        type: "GET",
                        dataType: "json",
                        success:function(data) {
                            $('#id_kecamatan').empty();
                            $('#id_kecamatan').append('<option>- Select District -</option>');
                            $.each(data, function(key, value) {
                                $('#id_kecamatan').append('<option value="'+ value.id +'">'+ value.nama +'</option>');
                            });

                            $('#id_kecamatan').selectpicker('refresh');
                        }
                    });
                } else {
                    $('select[name="id_kecamatan"]').empty();
                }
            });

            $('select[name="id_kecamatan"]').on('change', function() {
                var stateID = $(this).val();
                if(stateID) {
                    $.ajax({
                        url: "{{ url('/student/gethighschool') }}" + '/' + stateID,
                        type: "GET",
                        dataType: "json",
                        success:function(data) {
                            $('#sma').empty();
                            $('#sma').append('<option>- Select High School -</option>');
                            $.each(data, function(key, value) {
                                $('#sma').append('<option value="'+ value.nama +'">'+ value.nama +'</option>');
                            });

                            $('#sma').selectpicker('refresh');
                        }
                    });
                } else {
                    $('select[name="sma"]').empty();
                }
            });
    </script>
<script src="{{asset('public/assets/js/pages/forms/basic-form-elements.js')}}"></script>
<script src="{{asset('public/assets/js/pages/forms/advanced-form-elements.js')}}"></script>

    
@endsection