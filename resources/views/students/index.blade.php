@extends('layouts.master')
@section('title', 'Student List')
@section('content')

    <section class="content">
        <div class="container-fluid">
            
            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>TABEL MAHASISWA</h2>
                        </div>
                        <div class="body">
                            <p>
                                <button type="button" onclick="loadModal(this)" target="{{url('student/add-import')}}" class="btn btn-info waves-effect">Import Data</button>
                            </p>
                            <div class="table-responsive">

                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>NRP</th>
                                            <th>Nama</th>
                                            <th>Jenis Kelamin</th>
                                            <th>Jurusan</th>
                                            <th>Alamat</th>
                                            <th>Kecamatan</th>
                                            <th>Jalur Penerimaan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($data as $num => $item)
                                            <tr>
                                                <td>{{ $item->nrp }}</td>
                                                <td>{{ $item->nama }}</td>
                                                <td>{{ $item->jenis_kelamin }}</td>
                                                <td>{{ $item->program_studi }}</td>
                                                <td>{{ $item->alamat }}</td>
                                                <td>{{ $item->getKecamatan->nama }}, {{ $item->getKecamatan->getKota->nama }}</td>
                                                <td>{{ $item->jalur_penerimaan }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Basic Examples -->
        </div>
    </section>

@endsection

@section('scripts')
<script src="{{asset('public/assets/js/pages/tables/jquery-datatable.js')}}"></script>
@endsection