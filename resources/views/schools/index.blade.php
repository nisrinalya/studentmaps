@extends('layouts.master')
@section('title', 'School List')
@section('content')

    <section class="content">
        <div class="container-fluid">
            
            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>TABEL SEKOLAH</h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>NPSN</th>
                                            <th>Nama</th>
                                            <th>Status</th>
                                            <th>Kategori</th>
                                            <th>Alamat</th>
                                            <th>Kecamatan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($data as $num => $item)
                                            <tr>
                                                <td>{{ $item->npsn }}</td>
                                                <td>{{ $item->nama }}</td>
                                                <td>{{ $item->status }}</td>
                                                <td>{{ $item->kategori }}</td>
                                                <td>{{ $item->alamat }}</td>
                                                <td>{{ $item->getKecamatan->nama }}, {{ $item->getKecamatan->getKota->nama }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Basic Examples -->
        </div>
    </section>

@endsection

@section('scripts')
<script src="{{asset('public/assets/js/pages/tables/jquery-datatable.js')}}"></script>
@endsection