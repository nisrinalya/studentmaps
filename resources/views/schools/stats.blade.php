@extends('layouts.master')
@section('title', 'School Statistic')
@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>STATISTIK JUMLAH SEKOLAH</h2>
            </div>

            <!-- Pie Chart -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="body">
                           <div id="pie_chart" style="max-width: 900px"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Pie Chart -->

        </div>
    </section>

@endsection
@section('scripts')
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script>
    $(document).ready(function() {
        $.ajax({
            type: 'GET',
            url: "{{url('school/chart')}}",
            dataType: 'json',
            success: function (data) {
                console.log(data);
                getTransactionFraud(data);
            }
        });

    });

    // function getTransactionFraud(data) {
    //     // Build the chart
    //     Highcharts.chart('pie_chart', {
    //         chart: {
    //             plotBackgroundColor: null,
    //             plotBorderWidth: null,
    //             plotShadow: false,
    //             type: 'pie'
    //         },
    //         title: {
    //             text: 'High Schools Data of East Java'
    //         },
    //         tooltip: {
    //             pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    //         },
    //         plotOptions: {
    //             pie: {
    //                 allowPointSelect: true,
    //                 cursor: 'pointer',
    //                 dataLabels: {
    //                     enabled: true
    //                 },
    //                 showInLegend: false
    //             }
    //         },
    //         series: [{
    //             name: 'Number of High Schools',
    //             colorByPoint: true,
    //             data: data.data
    //         }]
    //     });
    // }

    function getTransactionFraud(data) {
        // Create the chart
        Highcharts.chart('pie_chart', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Data SMA/SMK Sederajat di Jawa Timur'
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Total jumlah SMA/SMK sederajat'
                }

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y}'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.city}</span>: <b>{point.y}</b><br/>'
            },

            series: [
                {
                    name: "Jumlah SMA/SMK Sederajat",
                    colorByPoint: false,
                    data: data.data
                }
            ]
        });
    }
</script>
@endsection