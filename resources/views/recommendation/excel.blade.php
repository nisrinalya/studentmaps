<table style="font-size:12px;border:2px solid #000;border-collapse:collapse">

    <tr style="border:2px solid #000">
        <td><b>Dokumen</b></td>
        <td><b>{{$title}}</b></td>
    </tr>
</table>

<table style="font-size:12px;border:2px solid #000;border-collapse:collapse">
    <tr style="border:1px solid #000;background: #e6faba">
        <th><b>Nama Kota</b></th>
        <th><b>Jumlah Sekolah</b></th>
        <th><b>Jumlah Mahasiswa</b></th>
        <th><b>Jumlah Sosialisasi</b></th>
        <th><b>DERAJAT KEANGGOTAAN {{str_replace('_',' ',$value1)}}</b></th>
        <th><b>DERAJAT KEANGGOTAAN {{str_replace('_',' ',$value2)}}</b></th>
        <th><b>DERAJAT KEANGGOTAAN {{str_replace('_',' ',$value3)}}</b></th>
        <!-- <th>DERAJAT KEANGGOTAAN SEKOLAH</th>
        <th>DERAJAT KEANGGOTAAN MAHASISWA</th>
        <th>DERAJAT KEANGGOTAAN SOSIALISASI</th> -->
        <th><b>HASIL FUZZY</b></th>
    </tr>
     @php
      $no=1;
     @endphp
    @foreach($data as $key =>$item)
         <tr>
             <td>{{ $item['nama_kota'] }}</td>
             <td>{{ $item['count_sekolah'] }}</td>
             <td>{{ $item['count_mahasiswa'] }}</td>
             <td>{{ $item['count_sosialisasi'] }}</td>
             <td>{{ $item[$value1] }}</td>
             <td>{{ $item[$value2] }}</td>
             <td>{{ $item[$value3] }}</td>
             <td style="background:#00ff00 "><b>{{ $item['result_value'] }}</b></td>
         </tr>

    @endforeach
    @php
    $no++
    @endphp
</table>