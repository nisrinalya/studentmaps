@extends('layouts.master')
@section('title', 'Recommendation')
@section('content')

    <section class="content">
        <div class="container-fluid">
            
            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>RECOMMENDATION</h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>Nama Kota</th>
                                            <th class="bg-blue-grey">Jumlah Sekolah</th>
                                            <th>Sangat Rendah</th>
                                            <th>Rendah</th>
                                            <th>Cukup</th>
                                            <th>Tinggi</th>
                                            <th>Sangat Tinggi</th>
                                            <th class="bg-blue-grey">Jumlah Mahasiswa</th>
                                            <th>Sangat Rendah</th>
                                            <th>Rendah</th>
                                            <th>Cukup</th>
                                            <th>Tinggi</th>
                                            <th>Sangat Tinggi</th>
                                            <th class="bg-blue-grey">Jumlah Sosialisasi</th>
                                            <th>Jarang</th>
                                            <th>Sering</th>
                                            <th class="bg-orange">HASIL FUZZY</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($result as $item)
                                            <tr>
                                                <th>{{ $item['nama_kota'] }}</th>
                                                <th class="bg-grey">{{ $item['count_sekolah'] }}</th>
                                                <th>{{ $item['srendah_sekolah'] }}</th>
                                                <th>{{ $item['rendah_sekolah'] }}</th>
                                                <th>{{ $item['cukup_sekolah'] }}</th>
                                                <th>{{ $item['tinggi_sekolah'] }}</th>
                                                <th>{{ $item['stinggi_sekolah'] }}</th>
                                                <th class="bg-grey">{{ $item['count_mahasiswa'] }}</th>
                                                <th>{{ $item['srendah_mahasiswa'] }}</th>
                                                <th>{{ $item['rendah_mahasiswa'] }}</th>
                                                <th>{{ $item['cukup_mahasiswa'] }}</th>
                                                <th>{{ $item['tinggi_mahasiswa'] }}</th>
                                                <th>{{ $item['stinggi_mahasiswa'] }}</th>
                                                <th class="bg-grey">{{ $item['count_sosialisasi'] }}</th>
                                                <th>{{ $item['jarang_sosialisasi'] }}</th>
                                                <th>{{ $item['sering_sosialisasi'] }}</th>
                                                <th class="bg-amber">{{ $item['fuzzy'] }}</th>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Basic Examples -->
        </div>
    </section>

@endsection

@section('scripts')
<script src="{{asset('public/assets/js/pages/tables/jquery-datatable.js')}}"></script>
@endsection