<style>
    #map { 
        width: 100%; 
        height: 500px; 
    }

    #map1 { 
        width: 100%; 
        height: 500px; 
    }

    #map2 { 
        width: 100%; 
        height: 500px; 
    }

    .info { 
        padding: 6px 8px; 
        font: 14px/16px Arial, Helvetica, sans-serif; 
        background: white; 
        background: rgba(255,255,255,0.8); 
        box-shadow: 0 0 15px rgba(0,0,0,0.2); 
        border-radius: 5px; 
    } 
    .info h4 { 
        margin: 0 0 5px; 
        color: #777; 
    }
    .legend { 
        text-align: left; 
        line-height: 18px; 
        color: #555; 
    } 
    .legend i { 
        width: 18px; 
        height: 18px; 
        float: left;
        margin-right: 8px; 
        opacity: 0.7; 
    }
</style>

<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="body">
                <div class="table-responsive">
                    <div class="row">
                        <div class="col-md-12">
                            <form class="form-horizontal" onsubmit="return false" id="form-konten">
                                <div class="btn-group">
                                    <button type="button" class="btn bg-blue dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">PILIH JENIS PETA SEBARAN <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li><a onclick="setView(1)">Peta Sebaran SMA/SMK Sederajat</a></li>
                                        <li><a onclick="setView(2)">Peta Sebaran Mahasiswa PENS</a></li>
                                        <li><a onclick="setView(3)">Peta Sebaran Riwayat Sosialisasi</a></li>
                                    </ul>
                                </div>

                                <!-- <div class="btn-group">
                                    <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span class="caret"></span>
                                        <span class="sr-only">Pilih Peta</span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li><a onclick="setView(1)">Peta Sebaran SMA/SMK Sederajat</a></li>
                                        <li><a onclick="setView(2)">Peta Sebaran Mahasiswa PENS</a></li>
                                        <li><a onclick="setView(3)">Peta Sebaran Riwayat Sosialisasi</a></li>
                                    </ul>
                                </div> -->
                            </form>
                        </div>
                    </div>
                    <div id="map-container">
                        <h4>Peta Sebaran SMA/SMK Sederajat</h4>
                        <div id="map"></div>
                    </div>
                    <div id="map-container1" style="display: none;">
                        <h4>Peta Sebaran Mahasiswa PENS</h4>
                        <div id="map1"></div>
                    </div>
                    <div id="map-container2" style="display: none;">
                        <h4>Peta Sebaran Riwayat Sosialisasi</h4>
                        <div id="map2"></div>
                    </div>
                    @php
                    $no=1;
                    @endphp
                    @foreach($limitData as $key => $item1)
                    <div style="background-color: #f5f5f5; padding: 100px; border-radius: 10px; margin-bottom: 20px; margin-top: 50px;">
                        <h4 style="text-align: center;">Daerah Terpilih {{$no}} :</h4>
                        <h2 style="text-align: center;">{{$item1['nama_kota']}}</h2>
                        <div class="row">
                            <div class="col-md-4" style="text-align: right;">
                                <h3><span class="label bg-pink">{{$item1['count_mahasiswa']}} Mahasiswa</span></h3>
                            </div>
                            <div class="col-md-4" style="text-align: center;">
                                <h3><span class="label bg-amber">{{$item1['count_sosialisasi']}} Riwayat Sosialisasi</span></h3>
                            </div>
                            <div class="col-md-4" style="text-align: left;">
                                <h3><span class="label bg-deep-orange">{{$item1['count_sekolah']}} Sekolah</span></h3>
                            </div>
                        </div>
                    </div>
                        @php
                            $no++;
                        @endphp
                    @endforeach

                    <div class="row">
                        <center><a onclick="exportData()" class="btn btn-success m-t-15 waves-effect" >Download Excel Rekomendasi</a> </center>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function exportData() {
        var query = {
            optSekolah: "{{$value1}}",
            optMahasiswa:"{{$value2}}",
            optSosialisasi:"{{$value3}}",
            year:"{{$year}}",
            optCount1:"{{$optCount1}}",
            optCount2:"{{$optCount2}}"
        }
        var url = "{{url('recommendation/excel?')}}" + $.param(query)

        window.location = url;
    }
    $.getJSON("{{asset('public/assets/json/kabkot_jatim.json')}}", function (data) {
        console.log(data)
        var map = L.map('map').setView([-7.536064,112.238402], 12);

        L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
            maxZoom: 18,
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
            '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
            'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            id: 'mapbox.streets'
        }).addTo(map);

        geojson = L.geoJson(data, {
            style: style,
            onEachFeature: onEachFeature
        }).addTo(map);

        // control that shows state info on hover
        var info = L.control();

        info.onAdd = function (map) {
            this._div = L.DomUtil.create('div', 'info');
            this.update();
            return this._div;
        };

        info.update = function (props) {
            this._div.innerHTML = '<h4>Provinsi Jawa Timur</h4>' +  (props ?
                    '<b>' + props.kabupaten+ '</b><br /><b>Jumlah Sekolah : </b>' + getCountSekolah(props.kabupaten) + ''
                    : 'Data Sekolah'
            );
        };

        info.addTo(map);

        function getColor(d) {
            return getCountSekolah(d) > 200  ? '#E31A1C' :
                getCountSekolah(d) > 100  ? '#FC4E2A' :
                    getCountSekolah(d) > 50   ? '#FD8D3C' :
                        getCountSekolah(d) > 20   ? '#FEB24C' :
                            getCountSekolah(d) > 10   ? '#FED976' :
                                '#FFEDA0';
        }

        function getCountSekolah(d) {
            @foreach($dataMaps as $key =>$item)
            if(d=="{{$item['nama_kota']}}"){
                return {{$item['count_sekolah']}};
            }
            @endforeach
        }

        function style(feature) {
            return {
                weight: 2,
                opacity: 1,
                color: 'white',
                dashArray: '3',
                fillOpacity: 0.7,
                fillColor: getColor(feature.properties.kabupaten)
            };
        }

        function highlightFeature(e) {
            var layer = e.target;

            layer.setStyle({
                weight: 5,
                color: '#666',
                dashArray: '',
                fillOpacity: 0.7
            });

            if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
                layer.bringToFront();
            }

            info.update(layer.feature.properties);
        }

        var geojson;

        function resetHighlight(e) {
            geojson.resetStyle(e.target);
            info.update();
        }

        function zoomToFeature(e) {
            map.fitBounds(e.target.getBounds());
            map.invalidateSize();
        }

        function onEachFeature(feature, layer) {
            layer.on({
                mouseover: highlightFeature,
                mouseout: resetHighlight,
                click: zoomToFeature
            });
        }

        function getColorName(d) {
            return d > 200  ? '#E31A1C' :
                d > 100  ? '#FC4E2A' :
                    d > 50   ? '#FD8D3C' :
                        d > 20   ? '#FEB24C' :
                            d > 10   ? '#FED976' :
                                '#FFEDA0';
        }

        map.attributionControl.addAttribution('Population data &copy; <a href="http://census.gov/">School</a>');

        var legend = L.control({position: 'bottomright'});

        legend.onAdd = function (map) {
            var div = L.DomUtil.create('div', 'info legend'),
                grades = [0, 10, 20, 50, 100, 200],
                labels = [],
                from, to;

            for (var i = 0; i < grades.length; i++) {
                from = grades[i];
                to = grades[i + 1];

                labels.push(
                    '<i style="background:' + getColorName(from + 1) + '"></i> ' +
                    from + (to ? '&ndash;' + to : '+'));
            }

            div.innerHTML = labels.join('<br>');
            return div;
        };

        legend.addTo(map);
    });

    function getMap() {
        $.getJSON("{{asset('public/assets/json/kabkot_jatim.json')}}", function (data) {
            console.log(data)
            var map = L.map('map').setView([-7.536064,112.238402], 12);

            L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
                maxZoom: 18,
                attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
                '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
                'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
                id: 'mapbox.streets'
            }).addTo(map);

            geojson = L.geoJson(data, {
                style: style,
                onEachFeature: onEachFeature
            }).addTo(map);

            // control that shows state info on hover
            var info = L.control();

            info.onAdd = function (map) {
                this._div = L.DomUtil.create('div', 'info');
                this.update();
                return this._div;
            };

            info.update = function (props) {
                this._div.innerHTML = '<h4>Provinsi Jawa Timur</h4>' +  (props ?
                        '<b>' + props.kabupaten+ '</b><br /><b>Jumlah Sekolah : </b>' + getCountSekolah(props.kabupaten) + ''
                        : 'Data Sekolah'
                );
            };

            info.addTo(map);

            function getColor(d) {
                return getCountSekolah(d) > 200  ? '#E31A1C' :
                    getCountSekolah(d) > 100  ? '#FC4E2A' :
                        getCountSekolah(d) > 50   ? '#FD8D3C' :
                            getCountSekolah(d) > 20   ? '#FEB24C' :
                                getCountSekolah(d) > 10   ? '#FED976' :
                                    '#FFEDA0';
            }

            function getCountSekolah(d) {
                @foreach($dataMaps as $key =>$item)
                if(d=="{{$item['nama_kota']}}"){
                    return {{$item['count_sekolah']}};
                }
                @endforeach
            }

            function style(feature) {
                return {
                    weight: 2,
                    opacity: 1,
                    color: 'white',
                    dashArray: '3',
                    fillOpacity: 0.7,
                    fillColor: getColor(feature.properties.kabupaten)
                };
            }

            function highlightFeature(e) {
                var layer = e.target;

                layer.setStyle({
                    weight: 5,
                    color: '#666',
                    dashArray: '',
                    fillOpacity: 0.7
                });

                if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
                    layer.bringToFront();
                }

                info.update(layer.feature.properties);
            }

            var geojson;

            function resetHighlight(e) {
                geojson.resetStyle(e.target);
                info.update();
            }

            function zoomToFeature(e) {
                map.fitBounds(e.target.getBounds());
                map.invalidateSize();
            }

            function onEachFeature(feature, layer) {
                layer.on({
                    mouseover: highlightFeature,
                    mouseout: resetHighlight,
                    click: zoomToFeature
                });
            }

            function getColorName(d) {
                return d > 200  ? '#E31A1C' :
                    d > 100  ? '#FC4E2A' :
                        d > 50   ? '#FD8D3C' :
                            d > 20   ? '#FEB24C' :
                                d > 10   ? '#FED976' :
                                    '#FFEDA0';
            }

            map.attributionControl.addAttribution('Population data &copy; <a href="http://census.gov/">School</a>');

            var legend = L.control({position: 'bottomright'});

            legend.onAdd = function (map) {
                var div = L.DomUtil.create('div', 'info legend'),
                    grades = [0, 10, 20, 50, 100, 200],
                    labels = [],
                    from, to;

                for (var i = 0; i < grades.length; i++) {
                    from = grades[i];
                    to = grades[i + 1];

                    labels.push(
                        '<i style="background:' + getColorName(from + 1) + '"></i> ' +
                        from + (to ? '&ndash;' + to : '+'));
                }

                div.innerHTML = labels.join('<br>');
                return div;
            };

            legend.addTo(map);
        });
    }
</script>

<script>
    function getMap1() {
        $.getJSON("{{asset('public/assets/json/kabkot_jatim.json')}}", function (data) {
            var map1 = L.map('map1').setView([-7.536064,112.238402], 12);

            L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
                maxZoom: 18,
                attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
                '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
                'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
                id: 'mapbox.streets'
            }).addTo(map1);

            map1.invalidateSize();

            geojson1 = L.geoJson(data, {
                style: style1,
                onEachFeature: onEachFeature1
            }).addTo(map1);

            // control that shows state info on hover
            var info1 = L.control();

            info1.onAdd = function (map1) {
                this._div = L.DomUtil.create('div', 'info');
                this.update();
                return this._div;
            };

            info1.update = function (props) {
                this._div.innerHTML = '<h4>Provinsi Jawa Timur</h4>' +  (props ?
                        '<b>' + props.kabupaten+ '</b><br /><b>Jumlah Sekolah : </b>' + getCountMahasiswa(props.kabupaten) + ''
                        : 'Data Mahasiswa'
                );
            };

            info1.addTo(map1);

            function getColor1(d) {
                return getCountMahasiswa(d) > 200  ? '#E31A1C' :
                    getCountMahasiswa(d) > 100  ? '#FC4E2A' :
                        getCountMahasiswa(d) > 50   ? '#FD8D3C' :
                            getCountMahasiswa(d) > 20   ? '#FEB24C' :
                                getCountMahasiswa(d) > 10   ? '#FED976' :
                                    '#FFEDA0';
            }

            function getCountMahasiswa(d) {
                @foreach($dataMaps as $key =>$item)
                if(d=="{{$item['nama_kota']}}"){
                    return {{$item['count_mahasiswa']}};
                }
                @endforeach
            }

            function style1(feature) {
                return {
                    weight: 2,
                    opacity: 1,
                    color: 'white',
                    dashArray: '3',
                    fillOpacity: 0.7,
                    fillColor: getColor1(feature.properties.kabupaten)
                };
            }

            function highlightFeature1(e) {
                var layer = e.target;

                layer.setStyle({
                    weight: 5,
                    color: '#666',
                    dashArray: '',
                    fillOpacity: 0.7
                });

                if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
                    layer.bringToFront();
                }

                info1.update(layer.feature.properties);
            }

            var geojson1;

            function resetHighlight1(e) {
                geojson1.resetStyle(e.target);
                info1.update();
            }

            function zoomToFeature1(e) {
                map1.fitBounds(e.target.getBounds());
                map1.invalidateSize();
            }

            function onEachFeature1(feature, layer) {
                layer.on({
                    mouseover: highlightFeature1,
                    mouseout: resetHighlight1,
                    click: zoomToFeature1
                });
            }

            function getColorName1(d) {
                return d > 200  ? '#E31A1C' :
                    d > 100  ? '#FC4E2A' :
                        d > 50   ? '#FD8D3C' :
                            d > 20   ? '#FEB24C' :
                                d > 10   ? '#FED976' :
                                    '#FFEDA0';
            }

            map1.attributionControl.addAttribution('Population data &copy; <a href="http://census.gov/">Student</a>');

            var legend1 = L.control({position: 'bottomright'});

            legend1.onAdd = function (map1) {

                var div = L.DomUtil.create('div', 'info legend'),
                    grades = [0, 10, 20, 50, 100, 200],
                    labels = [],
                    from, to;

                for (var i = 0; i < grades.length; i++) {
                    from = grades[i];
                    to = grades[i + 1];

                    labels.push(
                        '<i style="background:' + getColorName1(from + 1) + '"></i> ' +
                        from + (to ? '&ndash;' + to : '+'));
                }

                div.innerHTML = labels.join('<br>');
                return div;
            };

            legend1.addTo(map1);
            map1.invalidateSize();
        });
    }
</script>

<script>
    function getMap2() {
        $.getJSON("{{asset('public/assets/json/kabkot_jatim.json')}}", function (data) {
            var map2 = L.map('map2').setView([-7.536064,112.238402], 12);

            L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
                maxZoom: 18,
                attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
                '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
                'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
                id: 'mapbox.streets'
            }).addTo(map2);

            map2.invalidateSize();

            geojson2 = L.geoJson(data, {
                style: style2,
                onEachFeature: onEachFeature2
            }).addTo(map2);

            // control that shows state info on hover
            var info2 = L.control();

            info2.onAdd = function (map1) {
                this._div = L.DomUtil.create('div', 'info');
                this.update();
                return this._div;
            };

            info2.update = function (props) {
                this._div.innerHTML = '<h4>Provinsi Jawa Timur</h4>' +  (props ?
                        '<b>' + props.kabupaten+ '</b><br /><b>Jumlah Riwayat Sosialisasi : </b>' + getCountSosialisasi(props.kabupaten) + ''
                        : 'Data Riwayat Sosialisasi'
                );
            };

            info2.addTo(map2);

            function getColor2(d) {
                return getCountSosialisasi(d) > 6   ? '#FD8D3C' :
                    getCountSosialisasi(d) > 4   ? '#FEB24C' :
                        getCountSosialisasi(d) > 2   ? '#FED976' :
                            '#FFEDA0';
            }

            function getCountSosialisasi(d) {
                @foreach($dataMaps as $key =>$item)
                if(d=="{{$item['nama_kota']}}"){
                    return {{$item['count_sosialisasi']}};
                }
                @endforeach
            }

            function style2(feature) {
                return {
                    weight: 2,
                    opacity: 1,
                    color: 'white',
                    dashArray: '3',
                    fillOpacity: 0.7,
                    fillColor: getColor2(feature.properties.kabupaten)
                };
            }

            function highlightFeature2(e) {
                var layer = e.target;

                layer.setStyle({
                    weight: 5,
                    color: '#666',
                    dashArray: '',
                    fillOpacity: 0.7
                });

                if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
                    layer.bringToFront();
                }

                info2.update(layer.feature.properties);
            }

            var geojson2;

            function resetHighlight2(e) {
                geojson2.resetStyle(e.target);
                info2.update();
            }

            function zoomToFeature2(e) {
                map2.fitBounds(e.target.getBounds());
                map2.invalidateSize();
            }

            function onEachFeature2(feature, layer) {
                layer.on({
                    mouseover: highlightFeature2,
                    mouseout: resetHighlight2,
                    click: zoomToFeature2
                });
            }

            function getColorName2(d) {
                return d > 6   ? '#FD8D3C' :
                    d > 4   ? '#FEB24C' :
                        d > 2   ? '#FED976' :
                            '#FFEDA0';
            }

            map2.attributionControl.addAttribution('Population data &copy; <a href="http://census.gov/">Promotion History</a>');

            var legend2 = L.control({position: 'bottomright'});

            legend2.onAdd = function (map2) {
                var div = L.DomUtil.create('div', 'info legend'),
                    grades = [0, 2, 4, 6],
                    labels = [],
                    from, to;

                for (var i = 0; i < grades.length; i++) {
                    from = grades[i];
                    to = grades[i + 1];

                    labels.push(
                        '<i style="background:' + getColorName2(from + 1) + '"></i> ' +
                        from + (to ? '&ndash;' + to : '+'));
                }

                div.innerHTML = labels.join('<br>');
                return div;
            };

            legend2.addTo(map2);
            map2.invalidateSize();
        });
    }
</script>

<script>
    function setView(value) {
        if(value==1){
            getMap();
            $("#map-container").show();
            $("#map-container1").hide();
            $("#map-container2").hide();
        } else if(value==2){
            getMap1()
            $("#map-container1").show();
            $("#map-container2").hide();
            $("#map-container").hide();
        } else if(value==3){
            getMap2()
            $("#map-container2").show();
            $("#map-container").hide();
            $("#map-container1").hide();
        }
    }
</script>

<!-- <script type="text/javascript">
    function testFunction(value)
    {
        if(value==1){
            $("#map-container").show();
            $("#map-container1").hide();
        }else if(value==2){
            $("#map-container1").load(" #map-container1 > *");
            $("#map-container").hide();
            $("#map-container1").show();
        
        }
    }
</script> -->

<!-- <script type="text/javascript">
    // calling the maps

    var map = L.map('map').setView([-7.536064,112.238402], 12);

    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 18,
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
        '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
        'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        id: 'mapbox.streets'
    }).addTo(map);


    // control that shows state info on hover
    var info = L.control();

    info.onAdd = function (map) {
        this._div = L.DomUtil.create('div', 'info');
        this.update();
        return this._div;
    };

    // mouse point to polygon
    info.update = function (props) {
        this._div.innerHTML = '<h4>Provinsi Jawa Timur</h4>' +  (props ?
            '<b>' + props.kabupaten+ '</b><br /><b>Jumlah Sekolah : </b>' + getCountSekolah(props.kabupaten) + ''
            : 'Data Sekolah'
        );
    };

    info.addTo(map);


    // color to maps
    function getColor(d) {
        return getCountSekolah(d) > 200  ? '#E31A1C' :
                    getCountSekolah(d) > 100  ? '#FC4E2A' :
                        getCountSekolah(d) > 50   ? '#FD8D3C' :
                            getCountSekolah(d) > 20   ? '#FEB24C' :
                                getCountSekolah(d) > 10   ? '#FED976' :
                                    '#FFEDA0';
    }

    // calling the numbers
    function getCountSekolah(d) {
        @foreach($dataMaps as $key => $item)
            if(d=="{{$item['nama_kota']}}"){
                return {{$item['count_sekolah']}};
            }
        @endforeach
    }


    function style(feature) {
        return {
            weight: 2,
            opacity: 1,
            color: 'white',
            dashArray: '3',
            fillOpacity: 0.7,
            fillColor: getColor(feature.properties.kabupaten)
        };
    }

    function highlightFeature(e) {
        var layer = e.target;

        layer.setStyle({
            weight: 5,
            color: '#666',
            dashArray: '',
            fillOpacity: 0.7
        });

        if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
            layer.bringToFront();
        }

        info.update(layer.feature.properties);
    }

    var geojson;

    function resetHighlight(e) {
        geojson.resetStyle(e.target);
        info.update();
    }

    function zoomToFeature(e) {
        map.fitBounds(e.target.getBounds());
    }

    function onEachFeature(feature, layer) {
        layer.on({
            mouseover: highlightFeature,
            mouseout: resetHighlight,
            click: zoomToFeature
        });
    }

    function getColorName(d) {
        return d > 200  ? '#E31A1C' :
                    d > 100  ? '#FC4E2A' :
                        d > 50   ? '#FD8D3C' :
                            d > 20   ? '#FEB24C' :
                                d > 10   ? '#FED976' :
                                    '#FFEDA0';
    }

    $.getJSON("{{asset('public/assets/json/kabkot_jatim.json')}}", function (data) {
        geojson = L.geoJson(data, {
            style: style,
            onEachFeature: onEachFeature
        }).addTo(map);
    });


    map.attributionControl.addAttribution('Population data &copy; <a href="http://census.gov/">School</a>');

    var legend = L.control({position: 'bottomright'});

    legend.onAdd = function (map) {

        var div = L.DomUtil.create('div', 'info legend'),
            grades = [0, 10, 20, 50, 100, 200],
            labels = [],
            from, to;

        for (var i = 0; i < grades.length; i++) {
            from = grades[i];
            to = grades[i + 1];

            labels.push(
                '<i style="background:' + getColorName(from + 1) + '"></i> ' +
                from + (to ? '&ndash;' + to : '+'));
        }

        div.innerHTML = labels.join('<br>');
        return div;
    };

    legend.addTo(map);

</script>

<script type="text/javascript">
    // calling the maps
    
    var map1 = L.map('map1').setView([-7.536064,112.238402], 12);

    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 18,
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
        '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
        'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        id: 'mapbox.streets'
    }).addTo(map1);


    // control that shows state info on hover
    var info1 = L.control();

    info1.onAdd = function (map) {
        this._div = L.DomUtil.create('div', 'info');
        this.update();
        return this._div;
    };

    // mouse point to polygon
    info1.update = function (props) {
        this._div.innerHTML = '<h4>Provinsi Jawa Timur</h4>' +  (props ?
            '<b>' + props.kabupaten+ '</b><br /><b>Jumlah Mahasiswa : </b>' + getCountSekolah1(props.kabupaten) + ''
            : 'Data Mahasiswa'
        );
    };

    info1.addTo(map1);


    // color to maps
    function getColor1(d) {
        return getCountSekolah1(d) > 200  ? '#E31A1C' :
                    getCountSekolah1(d) > 100  ? '#FC4E2A' :
                        getCountSekolah1(d) > 50   ? '#FD8D3C' :
                            getCountSekolah1(d) > 20   ? '#FEB24C' :
                                getCountSekolah1(d) > 10   ? '#FED976' :
                                    '#FFEDA0';
    }

    // calling the numbers
    function getCountSekolah1(d) {
        @foreach($dataMaps as $key => $item)
            if(d=="{{$item['nama_kota']}}"){
                return {{$item['count_mahasiswa']}};
            }
        @endforeach
    }


    function style1(feature) {
        return {
            weight: 2,
            opacity: 1,
            color: 'white',
            dashArray: '3',
            fillOpacity: 0.7,
            fillColor: getColor1(feature.properties.kabupaten)
        };
    }

    function highlightFeature1(e) {
        var layer1 = e.target;

        layer1.setStyle({
            weight: 5,
            color: '#666',
            dashArray: '',
            fillOpacity: 0.7
        });

        if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
            layer1.bringToFront();
        }

        info1.update(layer1.feature.properties);
    }

    var geojson1;

    function resetHighlight1(e) {
        geojson1.resetStyle(e.target);
        info1.update();
    }

    function zoomToFeature1(e) {
        map1.fitBounds(e.target.getBounds());
    }

    function onEachFeature1(feature, layer) {
        layer.on({
            mouseover: highlightFeature1,
            mouseout: resetHighlight1,
            click: zoomToFeature1
        });
    }

    function getColorName1(d) {
        return d > 200  ? '#E31A1C' :
                    d > 100  ? '#FC4E2A' :
                        d > 50   ? '#FD8D3C' :
                            d > 20   ? '#FEB24C' :
                                d > 10   ? '#FED976' :
                                    '#FFEDA0';
    }
    $.getJSON("{{asset('public/assets/json/kabkot_jatim.json')}}", function (data) {
        geojson1 = L.geoJson(data, {
            style: style1,
            onEachFeature: onEachFeature1
        }).addTo(map1);
    });


    map1.attributionControl.addAttribution('Population data &copy; <a href="http://census.gov/">School</a>');

    var legend1 = L.control({position: 'bottomright'});

    legend1.onAdd = function (map1) {

        var div = L.DomUtil.create('div', 'info legend'),
            grades = [0, 10, 20, 50, 100, 200],
            labels = [],
            from, to;

        for (var i = 0; i < grades.length; i++) {
            from = grades[i];
            to = grades[i + 1];

            labels.push(
                '<i style="background:' + getColorName1(from + 1) + '"></i> ' +
                from + (to ? '&ndash;' + to : '+'));
        }

        div.innerHTML = labels.join('<br>');
        return div;
    };

    legend1.addTo(map1);

</script> -->