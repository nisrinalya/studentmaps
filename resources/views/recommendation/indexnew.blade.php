@extends('layouts.master')
@section('title', 'Recommendation')
@section('style')
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.0.3/dist/leaflet.css" />
<link rel="stylesheet" href="{{asset('public/assets/leaflet/leaflet.css')}}"/>
<link rel="stylesheet" href="{{asset('public/assets/leaflet/leaflet-search/src/leaflet-search.css')}}"/>
<link rel="stylesheet" href="{{asset('public/assets/leaflet/leaflet.defaultextent/dist/leaflet.defaultextent.css')}}"/>
<link rel="stylesheet" href="{{asset('public/assets/leaflet/leaflet-groupedlayercontrol/src/leaflet.groupedlayercontrol.css')}}"/>
@endsection
@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>REKOMENDASI</h2>
            </div>

            <!-- Maps -->
            <!-- <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="body">
                            <div id="map" style="width: 950px; height: 600px;"></div>
                        </div>
                    </div>
                </div>
            </div> -->

            <!-- Horizontal Layout -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="body">
                            <form class="form-horizontal" onsubmit="return false" id="form-konten">
                                <div class="row clearfix">
                                    <div class="col-md-2 form-control-label">
                                        <label for="nilai_sekolah">Nilai Sekolah</label>
                                    </div>
                                    <div class="col-md-7">
                                        <div class="form-group">
                                            <select class="form-control show-tick" name="optSekolah">
                                                <option>- Pilih Nilai Sekolah -</option>
                                                <option value="srendah_sekolah">Sangat Rendah</option>
                                                <option value="rendah_sekolah">Rendah</option>
                                                <option value="cukup_sekolah">Cukup</option>
                                                <option value="tinggi_sekolah">Tinggi</option>
                                                <option value="stinggi_sekolah">Sangat Tinggi</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <select class="form-control show-tick" name="optCount1">
                                                <option value="amp1">AND</option>
                                                <option value="sol1">OR</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-md-2 form-control-label">
                                        <label for="nilai_mahasiswa">Nilai Mahasiswa</label>
                                    </div>
                                    <div class="col-md-7">
                                        <div class="form-group">
                                            <select class="form-control show-tick" name="optMahasiswa">
                                                <option>- Pilih Nilai Mahasiswa -</option>
                                                <option value="srendah_mahasiswa">Sangat Rendah</option>
                                                <option value="rendah_mahasiswa">Rendah</option>
                                                <option value="cukup_mahasiswa">Cukup</option>
                                                <option value="tinggi_mahasiswa">Tinggi</option>
                                                <option value="stinggi_mahasiswa">Sangat Tinggi</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <select class="form-control show-tick" name="optCount2">
                                                <option value="amp2">AND</option>
                                                <option value="sol2">OR</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-md-2 form-control-label">
                                        <label for="nilai_sosialisasi">Nilai Sosialisasi</label>
                                    </div>
                                    <div class="col-md-7">
                                        <div class="form-group">
                                            <select class="form-control show-tick" name="optSosialisasi">
                                                <option>- Pilih Nilai Sosialisasi -</option>
                                                <option value="jarang_sosialisasi">Jarang</option>
                                                <option value="sering_sosialisasi">Sering</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-md-2 form-control-label">
                                        <label for="nilai_sosialisasi">Pilih Tahun</label>
                                    </div>
                                    <div class="col-md-7">
                                        <div class="form-group">
                                            <select class="form-control show-tick" name="year">
                                                <option>- Pilih Tahun -</option>
                                               @for($i=2015;$i<=2050;$i++)
                                                    <option value="{{$i}}">{{$i}}</option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
                                        <button type="submit" class="btn btn-success m-t-15 waves-effect">PROSES HITUNGAN</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div id="result"></div>
            <!-- #END# Horizontal Layout -->
        </div>
    </section>

@endsection

@section('scripts')
<script src="{{asset('public/assets/js/pages/forms/advanced-form-elements.js')}}"></script>
<script src="https://unpkg.com/leaflet@1.0.3/dist/leaflet.js"></script>
<script src="{{asset('public/assets/leaflet/leaflet.js')}}"></script>
<script src="{{asset('public/assets/leaflet/leaflet-providers/leaflet-providers.js')}}"></script>
<script src="{{asset('public/assets/leaflet/leaflet-ajax/dist/leaflet.ajax.js')}}"></script>
<script src="{{asset('public/assets/leaflet/leaflet-search/src/leaflet-search.js')}}"></script>
<script src="{{asset('public/assets/leaflet/leaflet.defaultextent/dist/leaflet.defaultextent.js')}}"></script>
<script src="{{asset('public/assets/leaflet/leaflet-groupedlayercontrol/src/leaflet.groupedlayercontrol.js')}}"></script>

<script type="text/javascript">
    $("#form-konten").submit(function(){
        var data=getFormData("form-konten");
        ajaxTransfer("{{url('recommendation/show')}}",data,function(result){
            $("#result").html(result);
        })
    })
</script>
@endsection