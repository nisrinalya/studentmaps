    <section class="home-banner-area">
        <div class="container">
            <div class="row justify-content-center fullscreen align-items-center">
                <div class="col-lg-5 col-md-8 home-banner-left">
                    <!-- <h1 class="text-white">Student<i>Maps</i></h1>
                    <p class="mx-auto text-white  mt-20 mb-40">
                        In the history of modern astronomy, there is probably no one greater leap forward than the building and launch of the space telescope known as the Hubble.
                    </p> -->
                </div>
                <div class="offset-lg-2 col-lg-5 col-md-12 home-banner-right">
                    <!-- <img class="img-fluid" src="{{asset('public/assets/frontend/img/header-img.png')}}" alt="" /> -->
                    <h1 class="text-white">P E N S<br>StudentMaps</h1>
                    <p class="mx-auto text-white  mt-20 mb-40">
                        Sistem informasi geografis sebaran mahasiswa PENS dan rekomendasi sosialisasi khusus wilayah Provinsi Jawa Timur
                    </p>
                </div>
            </div>
        </div>
    </section>