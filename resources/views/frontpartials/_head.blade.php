<head>
	<!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <!-- Favicon -->
    <link rel="shortcut icon" href="img/fav.png" />
    <!-- Author Meta -->
    <meta name="author" content="colorlib" />
    <!-- Meta Description -->
    <meta name="description" content="" />
    <!-- Meta Keyword -->
    <meta name="keywords" content="" />
    <!-- meta character set -->
    <meta charset="UTF-8" />

	<title>PENS StudentMaps</title>

	<link href="https://fonts.googleapis.com/css?family=Playfair+Display:900|Roboto:400,400i,500,700" rel="stylesheet" />
    <!-- CSS -->
    <link rel="stylesheet" href="{{asset('public/assets/frontend/css/linearicons.css')}}" />
    <link rel="stylesheet" href="{{asset('public/assets/frontend/css/font-awesome.min.css')}}" />
    <link rel="stylesheet" href="{{asset('public/assets/frontend/css/bootstrap.css')}}" />
    <link rel="stylesheet" href="{{asset('public/assets/frontend/css/magnific-popup.css')}}" />
    <link rel="stylesheet" href="{{asset('public/assets/frontend/css/owl.carousel.css')}}" />
    <link rel="stylesheet" href="{{asset('public/assets/frontend/css/nice-select.css')}}">
    <link rel="stylesheet" href="{{asset('public/assets/frontend/css/hexagons.min.css')}}" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/themify-icons/0.1.2/css/themify-icons.css')}}" />
    <link rel="stylesheet" href="{{asset('public/assets/frontend/css/main.css')}}" />

    @yield('frontstyle')
</head>