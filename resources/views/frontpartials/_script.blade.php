    <script src="{{asset('public/assets/frontend/js/vendor/jquery-2.2.4.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js')}}" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="{{asset('public/assets/frontend/js/vendor/bootstrap.min.js')}}"></script>
    <script src="{{asset('public/assets/frontend/js/jquery.ajaxchimp.min.js')}}"></script>
    <script src="{{asset('public/assets/frontend/js/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{asset('public/assets/frontend/js/parallax.min.js')}}"></script>
    <script src="{{asset('public/assets/frontend/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('public/assets/frontend/js/jquery.sticky.js')}}"></script>
    <script src="{{asset('public/assets/frontend/js/hexagons.min.js')}}"></script>
    <script src="{{asset('public/assets/frontend/js/jquery.counterup.min.js')}}"></script>
    <script src="{{asset('public/assets/frontend/js/waypoints.min.js')}}"></script>
    <script src="{{asset('public/assets/frontend/js/jquery.nice-select.min.js')}}"></script>
    <script src="{{asset('public/assets/frontend/js/main.js')}}"></script>

    @yield('frontscript')