@extends('layouts.landing')
@section('frontcontent')
	<!-- ================ start banner Area ================= -->
	<section class="banner-area" style="background: url({{ asset('public/assets/frontend/img/studentmaps_home_02.png') }}) no-repeat center; background-position: right; min-height: 510px;">
		<div class="container">
			<div class="row justify-content-center align-items-center">
				<div class="col-lg-12 banner-right">
					<h1 class="text-white">Statistik<br>Jumlah Riwayat Sosialisasi</h1>
				</div>
			</div>
		</div>
	</section>
	<!-- ================ End banner Area ================= -->

	<!-- Start Sample Area -->
	<section class="sample-text-area">
		<div class="container">
			<div id="pie_chart" style="max-width: 900px"></div>
		</div>
	</section>
	<!-- End Sample Area -->
@endsection
@section('frontscript')
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script>
    $(document).ready(function() {
        $.ajax({
            type: 'GET',
            url: "{{url('frontend/promotion-chart')}}",
            dataType: 'json',
            success: function (data) {
                console.log(data);
                getTransactionFraud(data);
            }
        });

    });

    function getTransactionFraud(data) {
        // Create the chart
        Highcharts.chart('pie_chart', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Data Riwayat Sosialisasi PENS per Tahun 2017-2019'
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Total jumlah riwayat sosialisasi'
                }

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y}'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.city}</span>: <b>{point.y}</b><br/>'
            },

            series: [
                {
                    name: "Jumlah Riwayat Sosialisasi",
                    colorByPoint: false,
                    data: data.data
                }
            ]
        });
    }
</script>
@endsection