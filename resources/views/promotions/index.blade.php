@extends('layouts.master')
@section('title', 'Promotion History List')
@section('content')

    <section class="content">
        <div class="container-fluid">
            
            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>TABEL RIWAYAT SOSIALISASI</h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>Penyelenggara</th>
                                            <th style="width: 60px;">Tanggal</th>
                                            <th>Alamat</th>
                                            <th>Kota</th>
                                            <th style="width: 80px;">Tahun Ajaran</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($data as $num => $item)
                                            <tr>
                                                <td>{{ $item->nama }}</td>
                                                <td>{{ $item->tanggal }}</td>
                                                <td>{{ $item->alamat }}</td>
                                                <td>{{ $item->getKota->nama }}</td>
                                                <td>{{ $item->ajaran_awal }}-{{ $item->ajaran_akhir }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Basic Examples -->
        </div>
    </section>

@endsection

@section('scripts')
<script src="{{asset('public/assets/js/pages/tables/jquery-datatable.js')}}"></script>
@endsection