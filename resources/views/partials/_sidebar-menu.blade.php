        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            @include('partials._user-info')
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu" style="background-image: url({{ asset('public/assets/images/bg-menu.png') }}); background-repeat: no-repeat; padding-top: 70px;">
                <ul class="list">
                    <!--<li class="header" style="background-color: transparent; color: transparent;">MAIN NAVIGATION</li>-->
                    <li class="{{ request()->is('/') ? 'active' : '' }}">
                        <a href="{{ url('/') }}">
                            <i class="material-icons">home</i>
                            <span>Dasbor</span>
                        </a>
                    </li>
                    <!-- <li class="{{ request()->is('school/add') || request()->is('student/add') || request()->is('promotion/add') ? 'active' : '' }}">
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">note_add</i>
                            <span>Add New Data</span>
                        </a>
                        <ul class="ml-menu">
                            <li class="{{ request()->is('student/add') ? 'active' : '' }}">
                                <a href="{{ url('student/add') }}">Add New Student Data</a>
                            </li>
                            <li class="{{ request()->is('promotion/add') ? 'active' : '' }}">
                                <a href="{{ url('promotion/add') }}">Add New Promotion History Data</a>
                            </li>
                            <li class="{{ request()->is('school/add') ? 'active' : '' }}">
                                <a href="{{ url('school/add') }}">Add New School Data</a>
                            </li>
                        </ul>
                    </li> -->
                    <li class="{{ request()->is('student') || request()->is('school') || request()->is('promotion') ? 'active' : '' }}">
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">view_list</i>
                            <span>Tabel</span>
                        </a>
                        <ul class="ml-menu">
                            <li class="{{ request()->is('student') ? 'active' : '' }}">
                                <a href="{{ url('student') }}">Tabel Mahasiswa</a>
                            </li>
                            <li class="{{ request()->is('promotion') ? 'active' : '' }}">
                                <a href="{{ url('promotion') }}">Tabel Riwayat Sosialisasi</a>
                            </li>
                            <li class="{{ request()->is('school') ? 'active' : '' }}">
                                <a href="{{ url('school') }}">Tabel Sekolah</a>
                            </li>
                        </ul>
                    </li>
                    <li class="{{ request()->is('student/statistic') || request()->is('promotion/statistic') || request()->is('school/statistic') ? 'active' : '' }}">
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">pie_chart</i>
                            <span>Statistik</span>
                        </a>
                        <ul class="ml-menu">
                            <li class="{{ request()->is('student/statistic') ? 'active' : '' }}">
                                <a href="{{ url('student/statistic') }}">Statistik Jumlah Mahasiswa</a>
                            </li>
                            <li class="{{ request()->is('promotion/statistic') ? 'active' : '' }}">
                                <a href="{{ url('promotion/statistic') }}">Statistik Jumlah Riwayat Sosialisasi</a>
                            </li>
                            <li class="{{ request()->is('school/statistic') ? 'active' : '' }}">
                                <a href="{{ url('school/statistic') }}">Statistik Jumlah Sekolah</a>
                            </li>
                        </ul>
                    </li>
                    <!-- <li class="{{ request()->is('recommendation/result') ? 'active' : '' }}">
                        <a href="{{ url('recommendation/result') }}">
                            <i class="material-icons">thumb_up</i>
                            <span>Recommendation</span>
                        </a>
                    </li> -->
                    <li class="{{ request()->is('recommendation/result2') ? 'active' : '' }}">
                        <a href="{{ url('recommendation/result2') }}">
                            <i class="material-icons">thumb_up</i>
                            <span>Rekomendasi</span>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            @include('partials._footer')
            <!-- #Footer -->
        </aside>

