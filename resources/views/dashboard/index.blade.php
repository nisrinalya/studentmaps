@extends('layouts.master')
@section('title', 'Dashboard')
@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>DASBOR</h2>
            </div>

            <!-- Pie Chart -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <!-- <div class="header">
                            <h2>STUDENTS STATISTIC</h2>
                        </div> -->
                        <div class="body">
                           <!--  <canvas id="pie_chart" height="100"></canvas> -->
                           <!-- <div id="pie_chart" style="min-width: 300px; height: 400px; margin: 0 auto"></div> -->
                           <!-- <div id="pie_chart" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div> -->
                           <div id="pie_chart" style="max-width: 900px"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Pie Chart -->

            <!-- Widgets -->
            <div class="row clearfix">
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="info-box-3 bg-pink hover-zoom-effect">
                        <div class="icon">
                            <i class="material-icons">group</i>
                        </div>
                        <div class="content">
                            <div class="text">MAHASISWA</div>
                            <div class="number">{{ $total_mahasiswa }}</div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="info-box-3 bg-amber hover-zoom-effect">
                        <div class="icon">
                            <i class="material-icons">flag</i>
                        </div>
                        <div class="content">
                            <div class="text">RIWAYAT SOSIALISASI</div>
                            <div class="number">{{ $total_sosialisasi }}</div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="info-box-3 bg-deep-orange hover-zoom-effect">
                        <div class="icon">
                            <i class="material-icons">school</i>
                        </div>
                        <div class="content">
                            <div class="text">SMA/SMK SEDERAJAT</div>
                            <div class="number">{{ $total_sekolah }}</div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Widgets -->

            <div class="row clearfix">
                <!-- Answered Tickets -->
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="card">
                        <div class="body bg-pink">
                            <div class="font-bold m-b--35">MAHASISWA</div>
                            <ul class="dashboard-stat-list">
                                @foreach($result_mahasiswa as $item)
                                    <li>{{ $item['nama_kota'] }}
                                        <span class="pull-right"><b>{{ $item['count_mahasiswa'] }}</b> <small>MAHASISWA</small></span></li>
                                @endforeach
                                <!-- <li>
                                    SURABAYA
                                    <span class="pull-right"><b>731</b> <small>STUDENTS</small></span>
                                </li>
                                <li>
                                    SIDOARJO
                                    <span class="pull-right"><b>556</b> <small>STUDENTS</small></span>
                                </li>
                                <li>
                                    GRESIK
                                    <span class="pull-right"><b>205</b> <small>STUDENTS</small></span>
                                </li>
                                <li>
                                    MOJOKERTO
                                    <span class="pull-right"><b>197</b> <small>STUDENTS</small></span>
                                </li>
                                <li>
                                    KEDIRI
                                    <span class="pull-right"><b>160</b> <small>STUDENTS</small></span>
                                </li> -->
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- #END# Answered Tickets -->
                <!-- Answered Tickets -->
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="card">
                        <div class="body bg-amber">
                            <div class="font-bold m-b--35">RIWAYAT SOSIALISASI</div>
                            <ul class="dashboard-stat-list">
                                @foreach($result_sosialisasi as $item)
                                    <li>{{ $item['nama_kota'] }}
                                        <span class="pull-right"><b>{{ $item['count_sosialisasi'] }}</b> <small>KALI</small></span></li>
                                @endforeach
                                <!-- <li>
                                    MOJOKERTO
                                    <span class="pull-right"><b>2</b> <small>TIMES</small></span>
                                </li>
                                <li>
                                    KEDIRI
                                    <span class="pull-right"><b>2</b> <small>TIMES</small></span>
                                </li>
                                <li>
                                    JOMBANG
                                    <span class="pull-right"><b>2</b> <small>TIMES</small></span>
                                </li>
                                <li>
                                    BLITAR
                                    <span class="pull-right"><b>2</b> <small>TIMES</small></span>
                                </li>
                                <li>
                                    BOJONEGORO
                                    <span class="pull-right"><b>2</b> <small>TIMES</small></span>
                                </li> -->
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- #END# Answered Tickets -->
                <!-- Answered Tickets -->
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="card">
                        <div class="body bg-deep-orange">
                            <div class="font-bold m-b--35">SMA/SMK SEDERAJAT</div>
                            <ul class="dashboard-stat-list">
                                @foreach($result_sekolah as $item)
                                    <li>{{ $item['nama_kota'] }}
                                        <span class="pull-right"><b>{{ $item['count_sekolah'] }}</b> <small>SEKOLAH</small></span></li>
                                @endforeach
                                <!-- <li>
                                    JEMBER
                                    <span class="pull-right"><b>387</b> <small>SCHOOLS</small></span>
                                </li>
                                <li>
                                    SURABAYA
                                    <span class="pull-right"><b>320</b> <small>SCHOOLS</small></span>
                                </li>
                                <li>
                                    MALANG
                                    <span class="pull-right"><b>278</b> <small>SCHOOLS</small></span>
                                </li>
                                <li>
                                    PAMEKASAN
                                    <span class="pull-right"><b>259</b> <small>SCHOOLS</small></span>
                                </li>
                                <li>
                                    SUMENEP
                                    <span class="pull-right"><b>258</b> <small>SCHOOLS</small></span>
                                </li> -->
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- #END# Answered Tickets -->
            </div>
        </div>
    </section>

@endsection
@section('scripts')
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script>
    $(document).ready(function() {
        $.ajax({
            type: 'GET',
            url: "{{url('backend/chart')}}",
            dataType: 'json',
            success: function (data) {
                console.log(data);
                getTransactionFraud(data);
            }
        });

    });

    // function getTransactionFraud(data) {
    //     // Build the chart
    //     Highcharts.chart('pie_chart', {
    //         chart: {
    //             plotBackgroundColor: null,
    //             plotBorderWidth: null,
    //             plotShadow: false,
    //             type: 'pie'
    //         },
    //         title: {
    //             text: 'PENS Students Data per Year 2015-2019'
    //         },
    //         tooltip: {
    //             pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    //         },
    //         plotOptions: {
    //             pie: {
    //                 allowPointSelect: true,
    //                 cursor: 'pointer',
    //                 dataLabels: {
    //                     enabled: true
    //                 },
    //                 showInLegend: false
    //             }
    //         },
    //         series: [{
    //             name: 'Number of Students',
    //             colorByPoint: true,
    //             data: data.data
    //         }]
    //     });
    // }

    function getTransactionFraud(data) {
        // Create the chart
        Highcharts.chart('pie_chart', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Data Riwayat Sosialisasi PENS per Tahun 2017-2019'
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Total jumlah riwayat sosialisasi'
                }

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y}'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.city}</span>: <b>{point.y}</b><br/>'
            },

            series: [
                {
                    name: "Jumlah Riwayat Sosialisasi",
                    colorByPoint: false,
                    data: data.data
                }
            ]
        });
    }
</script>
@endsection